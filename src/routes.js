// @mui icons
import Icon from '@mui/material/Icon';

// page components
import Home from 'pages/home';
import SignDetail from 'pages/manage-sign/detail';
import SignAlert from 'pages/manage-sign/alert';
import ManageVehicles from 'pages/manage-vehicles';
import Report from 'pages/report';

const routes = [
  {
    type: 'collapse',
    name: 'Brooklyn Alice',
    key: 'brooklyn-alice',
    icon: 'Avatar',
    collapse: [
      {
        type: 'logout',
        name: 'Logout',
        key: 'logout',
        route: 'sign-in',
        icon: <Icon fontSize='medium'>logout</Icon>,
      },
    ],
  },
  { type: 'divider', key: 'divider-0' },
  {
    type: 'menu',
    name: 'Home',
    key: 'home',
    route: '/',
    icon: <Icon fontSize='medium'>home</Icon>,
    component: <Home />,
    noCollapse: true,
  },
  { type: 'divider', key: 'divider-1' },
  {
    type: 'collapse',
    name: 'Sign Management',
    key: 'sign',
    icon: <Icon fontSize='medium'>video_label</Icon>,
    collapse: [
      {
        name: 'Sign Detail',
        key: 'detail',
        route: 'sign/detail',
        icon: <Icon fontSize='medium'>wysiwyg</Icon>,
        component: <SignDetail />,
      },
      {
        name: 'Sign Alert',
        key: 'alert',
        route: 'sign/alert',
        icon: <Icon fontSize='medium'>sms_failed</Icon>,
        component: <SignAlert />,
      },
    ],
  },
  {
    type: 'menu',
    name: 'Manage Vehicles',
    key: 'vehicles',
    route: 'manage-vehicles',
    icon: <Icon fontSize='medium'>directions_bus</Icon>,
    component: <ManageVehicles />,
    noCollapse: true,
  },
  {
    type: 'menu',
    name: 'Report',
    key: 'report',
    route: 'report',
    icon: <Icon fontSize='medium'>pending_actions</Icon>,
    component: <Report />,
    noCollapse: true,
  },
];

export default routes;
