import { createSlice } from '@reduxjs/toolkit';

// Slice
const sign = createSlice({
  name: 'sign',
  initialState: {
    medias: [],
    nav_view: [],
    reload: null,
  },
  reducers: {
    setMedias: (state, action) => {
      state.medias = action.payload;
    },
    setNavView: (state, action) => {
      state.nav_view = action.payload;
    },
    setReload: (state, action) => {
      state.reload = action.payload;
    },
  },
});

export default sign;
