import { createSlice } from '@reduxjs/toolkit';

// Slice
const map = createSlice({
  name: 'map',
  initialState: {
    bus_lines: [],
    vehicles_emit: [],
    public_vehicles_emit: [],
    public_vehicles_in: [],
    mappico_vehicles: [],
  },
  reducers: {
    setBusLine: (state, action) => {
      state.bus_lines = action.payload;
    },
    setVehiclesEmit: (state, action) => {
      state.vehicles_emit = action.payload;
    },
    setPublicVehiclesEmit: (state, action) => {
      state.public_vehicles_emit = action.payload;
    },
    setPublicVehiclesIn: (state, action) => {
      state.public_vehicles_in = action.payload;
    },
    setMappicoVehicles: (state, action) => {
      state.mappico_vehicles = action.payload;
    },
  },
});

export default map;
