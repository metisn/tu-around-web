import { createSlice } from '@reduxjs/toolkit';

// Slice
const auth = createSlice({
  name: 'auth',
  initialState: {
    user: {},
  },
  reducers: {
    loginSuccess: (state, action) => {
      state.user = action.payload;
    },
    logoutSuccess: (state) => {
      state.user = null;
    },
  },
});

export default auth;
