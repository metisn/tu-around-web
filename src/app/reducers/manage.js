import { createSlice } from '@reduxjs/toolkit';

// Slice
const manage = createSlice({
  name: 'manage',
  initialState: {
    bus_lines: [],
    vehicle_lists: [],
  },
  reducers: {
    setBusLine: (state, action) => {
      state.bus_lines = action.payload;
    },
    setVehicleLists: (state, action) => {
      state.vehicle_lists = action.payload;
    },
  },
});

export default manage;
