import { createSlice } from '@reduxjs/toolkit';

// Slice
const report = createSlice({
  name: 'report',
  initialState: {
    consumption: [],
  },
  reducers: {
    setConsumption: (state, action) => {
      state.consumption = action.payload;
    },
  },
});

export default report;
