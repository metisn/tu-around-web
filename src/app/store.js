import { configureStore } from '@reduxjs/toolkit';
import { combineReducers } from 'redux';

import auth from './reducers/auth';
import map from './reducers/map';
import sign from './reducers/sign';
import manage from './reducers/manage';
import report from './reducers/report';

const reducer = combineReducers({
  // here we will be adding reducers
  auth: auth.reducer,
  map: map.reducer,
  sign: sign.reducer,
  manage: manage.reducer,
  report: report.reducer,
});

export const store = configureStore({
  reducer,
});
