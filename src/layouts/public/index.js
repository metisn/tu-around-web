import PropTypes from 'prop-types';

// main layout container
import PublicContainer from 'layouts/public/PublicContainer';

const PublicLayout = ({ children, background }) => {
  return <PublicContainer background={background}>{children}</PublicContainer>;
};

PublicLayout.propTypes = {
  children: PropTypes.node.isRequired,
  background: PropTypes.string,
};

PublicLayout.defaultProps = {
  background: 'default',
};

export default PublicLayout;
