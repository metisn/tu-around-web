import { Fragment, useState, useEffect, useCallback } from 'react';
import PropTypes from 'prop-types';

// import components
import Navbar from 'components/Navbar';
import Sidenav from 'components/Sidenav';
import Footer from 'components/Footer';

// main layout container
import PrivateContainer from './PrivateContainer';

// Material Dashboard 2 PRO React contexts
import { useMaterialUIController, setMiniSidenav } from 'context';

// import custom hooks for socket connection
import { useSocket } from 'hooks';

// assets logo &* icon
import tuLogo from 'assets/images/tu-logo.webp';

const PrivateLayout = ({ navMenu, children }) => {
  const socket = useSocket();
  const [controller, dispatch] = useMaterialUIController();
  const { miniSidenav, sidenavColor } = controller;

  // state variables
  const [onMouseEnter, setOnMouseEnter] = useState(false);

  useEffect(() => {
    return () => {
      socket?.close();
    };
  }, [socket]);

  // Open sidenav when mouse enter on mini sidenav
  const handleOnMouseEnter = useCallback(() => {
    if (miniSidenav && !onMouseEnter) {
      setMiniSidenav(dispatch, false);
      setOnMouseEnter(true);
    }
  }, [dispatch, miniSidenav, onMouseEnter]);

  // Close sidenav when mouse leave mini sidenav
  const handleOnMouseLeave = useCallback(() => {
    if (onMouseEnter) {
      setMiniSidenav(dispatch, true);
      setOnMouseEnter(false);
    }
  }, [dispatch, onMouseEnter]);

  return (
    <Fragment>
      <Sidenav
        color={sidenavColor}
        brand={tuLogo}
        brandName='TU Around'
        routes={navMenu}
        onMouseEnter={handleOnMouseEnter}
        onMouseLeave={handleOnMouseLeave}
      />

      <PrivateContainer>
        <Navbar />
        {children}
        <Footer />
      </PrivateContainer>
    </Fragment>
  );
};

PrivateLayout.propTypes = {
  navMenu: PropTypes.array.isRequired,
  children: PropTypes.node.isRequired,
};

export default PrivateLayout;
