import { StrictMode } from 'react';
import ReactDOM from 'react-dom';
import App from './App';

// SW registry
import * as serviceWorker from './serviceWorker';

// MUI
import theme from 'theme';
import CssBaseline from '@mui/material/CssBaseline';
import { ThemeProvider } from '@mui/material/styles';

// react-toastify component
import { ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import 'override.css';

// Material Dashboard 2 PRO React Context Provider
import { MaterialUIControllerProvider } from 'context';

ReactDOM.render(
  <MaterialUIControllerProvider>
    <ThemeProvider theme={theme}>
      <CssBaseline />
      <StrictMode>
        <App />
      </StrictMode>
      <ToastContainer autoClose={1500} style={{ fontSize: '12px !important' }} />
    </ThemeProvider>
  </MaterialUIControllerProvider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
