function checkObjectEmpty(obj) {
  for (var key in obj) {
    // eslint-disable-next-line no-prototype-builtins
    if (obj.hasOwnProperty(key)) return false;
  }
  return true;
}

const checkIsEmpty = (input) => {
  if (typeof input === 'object') return checkObjectEmpty(input);

  if (
    !input ||
    input == null ||
    input === 'null' ||
    input === '' ||
    input === 'undefined' ||
    input === undefined ||
    input.length === 0
  ) {
    return true;
  }

  return false;
};

export default checkIsEmpty;
