export const toastOptions = {
  position: 'bottom-right',
  // autoClose: 5000,
  hideProgressBar: false,
  closeOnClick: false,
  closeButton: true,
  pauseOnHover: false,
  draggable: false,
  progress: undefined,
  pauseOnFocusLoss: false,
  // theme: 'colored',
};
