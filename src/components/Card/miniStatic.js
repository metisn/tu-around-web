/**
=========================================================
* Material Dashboard 2 PRO React - v2.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-pro-react
* Copyright 2022 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

// prop-types is a library for typechecking of props
import PropTypes from 'prop-types';

// @mui material components
import Card from '@mui/material/Card';
import Grid from '@mui/material/Grid';
import Icon from '@mui/material/Icon';

// Material Dashboard 2 PRO React components
import Box from 'components/Box';
import Typography from 'components/Typography';

// Material Dashboard 2 PRO React context
import { useMaterialUIController } from 'context';

function MiniStatisticsCard({ bgColor, title, count, icon, direction }) {
  const [controller] = useMaterialUIController();
  const { darkMode } = controller;

  return (
    <Card sx={{ overflow: 'hidden' }}>
      <Box
        bgColor={bgColor}
        variant='gradient'
        sx={({ palette: { background } }) => ({
          background: darkMode && background.card,
        })}
      >
        <Box p={2} sx={{ padding: '6px 16px !important' }}>
          <Grid container alignItems='center'>
            {direction === 'left' ? (
              <Grid item xs={4}>
                <Box
                  variant='gradient'
                  bgColor={bgColor === 'white' ? icon.color : 'white'}
                  color={bgColor === 'white' ? 'white' : 'dark'}
                  width='3rem'
                  height='3rem'
                  borderRadius='md'
                  display='flex'
                  justifyContent='center'
                  alignItems='center'
                  shadow='md'
                >
                  <Icon fontSize='medium' color='inherit'>
                    {icon.component}
                  </Icon>
                </Box>
              </Grid>
            ) : null}
            <Grid item xs={8}>
              <Box
                ml={direction === 'left' ? 2 : 0}
                lineHeight={1}
                textAlign={direction === 'left' ? 'right' : 'left'}
              >
                <Typography
                  variant='button'
                  color={icon.color}
                  opacity={bgColor === 'white' ? 1 : 0.7}
                  textTransform='capitalize'
                  fontWeight={title.fontWeight}
                >
                  {title.text}
                </Typography>
                <Typography
                  variant='h2'
                  fontWeight='bold'
                  color={bgColor === 'white' ? 'dark' : 'white'}
                >
                  {count}
                </Typography>
              </Box>
            </Grid>
            {direction === 'right' ? (
              <Grid item xs={4}>
                <Box
                  variant='gradient'
                  bgColor={bgColor === 'white' ? icon.color : 'white'}
                  color={bgColor === 'white' ? 'white' : 'dark'}
                  width='3rem'
                  height='3rem'
                  marginLeft='auto'
                  borderRadius='md'
                  display='flex'
                  justifyContent='center'
                  alignItems='center'
                  shadow='md'
                >
                  <Icon fontSize='medium' color='inherit'>
                    {icon.component}
                  </Icon>
                </Box>
              </Grid>
            ) : null}
          </Grid>
        </Box>
      </Box>
    </Card>
  );
}

// Setting default values for the props of MiniStatisticsCard
MiniStatisticsCard.defaultProps = {
  bgColor: 'white',
  title: {
    fontWeight: 'light',
    text: '',
  },
  percentage: {
    color: 'success',
    text: '',
  },
  direction: 'right',
};

// Typechecking props for the MiniStatisticsCard
MiniStatisticsCard.propTypes = {
  bgColor: PropTypes.oneOf([
    'white',
    'primary',
    'secondary',
    'info',
    'success',
    'warning',
    'error',
    'dark',
  ]),
  title: PropTypes.PropTypes.shape({
    fontWeight: PropTypes.oneOf(['light', 'regular', 'medium', 'bold']),
    text: PropTypes.string,
  }),
  count: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  percentage: PropTypes.shape({
    color: PropTypes.oneOf([
      'primary',
      'secondary',
      'info',
      'success',
      'warning',
      'error',
      'dark',
      'white',
    ]),
    text: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  }),
  icon: PropTypes.shape({
    color: PropTypes.oneOf(['primary', 'secondary', 'info', 'success', 'warning', 'error', 'dark']),
    component: PropTypes.node.isRequired,
  }).isRequired,
  direction: PropTypes.oneOf(['right', 'left']),
};

export default MiniStatisticsCard;
