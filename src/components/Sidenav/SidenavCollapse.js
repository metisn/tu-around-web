/**
=========================================================
* Material Dashboard 2 PRO React - v2.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-pro-react
* Copyright 2022 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

// prop-types is a library for typechecking of props.
import { useCallback } from 'react';
import PropTypes from 'prop-types';

// @mui material components
import Collapse from '@mui/material/Collapse';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import Icon from '@mui/material/Icon';

// Material Dashboard 2 PRO React components
import Box from 'components/Box';
import Avatar from 'components/Avatar';

// Custom styles for the SidenavCollapse
import {
  collapseItem,
  collapseIconBox,
  collapseIcon,
  collapseText,
  collapseArrow,
} from './styles/sidenavCollapse';

// Material Dashboard 2 PRO React context
import { useMaterialUIController } from 'context';

function SidenavCollapse({
  icon,
  name,
  children,
  active,
  noCollapse,
  open,
  isHome,
  item,
  ...rest
}) {
  const [controller] = useMaterialUIController();
  const { miniSidenav, whiteSidenav, darkMode } = controller;

  const user = JSON.parse(sessionStorage.getItem('_user_info'));

  const getUserAvatar = useCallback(({ name }) => {
    function stringToColor(string) {
      let hash = 0;
      let i;

      for (i = 0; i < string.length; i += 1) {
        hash = string.charCodeAt(i) + ((hash << 5) - hash);
      }

      let color = '#';

      for (i = 0; i < 3; i += 1) {
        const value = (hash >> (i * 8)) & 0xff;
        color += `00${value.toString(16)}`.slice(-2);
      }

      return color;
    }

    function stringAvatar(name) {
      return {
        sx: {
          bgcolor: stringToColor(name),
        },
        children: `${name.split(' ')[0][0].toUpperCase()}${name.split(' ')[1][0].toUpperCase()}`,
      };
    }

    return <Avatar {...stringAvatar(name)} alt={name} size='sm' />;
  }, []);

  return (
    <>
      <ListItem component='li'>
        <Box
          {...rest}
          sx={(theme) =>
            collapseItem(theme, { active, whiteSidenav, darkMode, isHome, item, noCollapse })
          }
        >
          <ListItemIcon
            sx={(theme) =>
              collapseIconBox(theme, { active, whiteSidenav, darkMode, isHome, item, noCollapse })
            }
          >
            {typeof icon === 'string' ? (
              icon === 'Avatar' ? (
                user && getUserAvatar(user)
              ) : (
                <Icon sx={(theme) => collapseIcon(theme, { active })}>{icon}</Icon>
              )
            ) : (
              icon
            )}
          </ListItemIcon>

          <ListItemText
            primary={name}
            sx={(theme) =>
              collapseText(theme, {
                miniSidenav,
                whiteSidenav,
                active,
                isHome,
                item,
                noCollapse,
              })
            }
          />

          <Icon
            sx={(theme) =>
              collapseArrow(theme, {
                noCollapse,
                whiteSidenav,
                miniSidenav,
                open,
                active,
                darkMode,
              })
            }
          >
            expand_less
          </Icon>
        </Box>
      </ListItem>
      {children && (
        <Collapse in={open} unmountOnExit>
          {children}
        </Collapse>
      )}
    </>
  );
}

// Setting default values for the props of SidenavCollapse
SidenavCollapse.defaultProps = {
  active: false,
  noCollapse: false,
  children: false,
  open: false,
};

// Typechecking props for the SidenavCollapse
SidenavCollapse.propTypes = {
  icon: PropTypes.node.isRequired,
  name: PropTypes.string.isRequired,
  children: PropTypes.node,
  active: PropTypes.bool,
  noCollapse: PropTypes.bool,
  open: PropTypes.bool,
  isHome: PropTypes.bool,
  item: PropTypes.bool,
};

export default SidenavCollapse;
