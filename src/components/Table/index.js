/* eslint-disable react/prop-types */
// @mui material components
import { memo } from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableContainer from '@mui/material/TableContainer';
import TableRow from '@mui/material/TableRow';

import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import { Skeleton } from '@mui/material';
import { styled } from '@mui/material/styles';
import { isEmpty } from 'utils';

function DataTable({
  TableBodyItem,
  headCellsData,
  bodyCellsData,
  filterData,
  handleAction,
  isLoading,
  emptyText,
}) {
  const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.white,
      color: theme.palette.text.main,
      fontWeight: 500,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));

  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));

  const LoadingRow = () => {
    return [...Array(1)].map((item, index) => {
      return (
        <StyledTableRow key={index}>
          {headCellsData.map((item, index) => {
            return (
              <TableCell key={index} sx={{ padding: '13px' }}>
                <Skeleton variant='text' />
              </TableCell>
            );
          })}
        </StyledTableRow>
      );
    });
  };

  return (
    <TableContainer sx={{ boxShadow: 'none' }}>
      <Table stickyHeader sx={{ minWidth: 750 }} aria-labelledby='tableTitle' size={'medium'}>
        <TableHead>
          <TableRow>
            {headCellsData.map((cell, index) => (
              <StyledTableCell key={cell.id || index} align={cell.align}>
                {cell.label}
              </StyledTableCell>
            ))}
          </TableRow>
        </TableHead>
        <TableBody>
          {isLoading ? (
            <LoadingRow />
          ) : isEmpty(filterData) ? (
            bodyCellsData.map((row, index) => (
              <StyledTableRow
                key={row.id || index}
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              >
                <TableBodyItem data={row} index={index + 1} callAction={handleAction} />
              </StyledTableRow>
            ))
          ) : (
            filterData.map((row, index) => (
              <StyledTableRow
                key={row.id || index}
                sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
              >
                <TableBodyItem data={row} index={index + 1} callAction={handleAction} />
              </StyledTableRow>
            ))
          )}

          {bodyCellsData.length < 1 && !isLoading && (
            <StyledTableRow>
              <TableCell
                colSpan={headCellsData.length}
                sx={{ padding: '13px', textAlign: 'center' }}
              >
                {emptyText}
              </TableCell>
            </StyledTableRow>
          )}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

// Setting default values for the props of DataTable
DataTable.defaultProps = {
  entriesPerPage: { defaultValue: 10, entries: [5, 10, 15, 20, 25] },
  canSearch: false,
  showTotalEntries: true,
  pagination: { variant: 'gradient', color: 'info' },
  isSorted: true,
  noEndBorder: false,
  emptyText: 'No Record',
};

// Typechecking props for the DataTable
DataTable.propTypes = {};

export default memo(DataTable);
