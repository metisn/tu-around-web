/**
=========================================================
* Material Dashboard 2 PRO React - v2.1.0
=========================================================

* Product Page: https://www.creative-tim.com/product/material-dashboard-pro-react
* Copyright 2022 Creative Tim (https://www.creative-tim.com)

Coded by www.creative-tim.com

 =========================================================

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/

// prop-types is a library for typechecking of props
import PropTypes from 'prop-types';

// @mui material components
import Link from '@mui/material/Link';

// Material Dashboard 2 PRO React components
import Box from 'components/Box';
import Typography from 'components/Typography';

// Material Dashboard 2 PRO React base styles
import typography from 'theme/base/typography';

function Footer({ company, links }) {
  const { href, name } = company;
  const { size } = typography;

  const renderLinks = () =>
    links.map((link) => (
      <Box key={link.name} component='li' px={2} lineHeight={1}>
        <Link href={link.href} target='_blank'>
          <Typography variant='button' fontWeight='regular' color='text'>
            {link.name}
          </Typography>
        </Link>
        <Link href={'/public/table'} target='_blank' ml={2}>
          <Typography variant='button' fontWeight='regular' color='text'>
            Public table
          </Typography>
        </Link>
      </Box>
    ));

  return (
    <Box
      width='100%'
      display='flex'
      flexDirection={{ xs: 'column', lg: 'row' }}
      justifyContent='space-between'
      alignItems='center'
      px={1.5}
      mt={1}
    >
      <Box
        display='flex'
        justifyContent='center'
        alignItems='center'
        flexWrap='wrap'
        color='text'
        fontSize={size.sm}
        px={1.5}
      >
        &copy; {new Date().getFullYear()} All right reserved,
        <Link href={href} target='_blank'>
          <Typography variant='button' fontWeight='medium'>
            &nbsp;{name}&nbsp;
          </Typography>
        </Link>
      </Box>
      <Box
        component='ul'
        sx={({ breakpoints }) => ({
          display: 'flex',
          flexWrap: 'wrap',
          alignItems: 'center',
          justifyContent: 'center',
          listStyle: 'none',
          mt: 3,
          mb: 0,
          p: 0,

          [breakpoints.up('lg')]: {
            mt: 0,
          },
        })}
      >
        {renderLinks()}
      </Box>
    </Box>
  );
}

// Setting default values for the props of Footer
Footer.defaultProps = {
  company: { href: 'http://gis.siit.tu.ac.th/index.html', name: 'GISLab' },
  links: [{ href: 'http://gis.siit.tu.ac.th/screens_en/contact_us.html', name: 'Contact Us' }],
};

// Typechecking props for the Footer
Footer.propTypes = {
  company: PropTypes.objectOf(PropTypes.string),
  links: PropTypes.arrayOf(PropTypes.object),
};

export default Footer;
