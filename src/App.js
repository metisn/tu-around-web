import { Suspense } from 'react';
import PropTypes from 'prop-types';
// import Cookies from 'js-cookie';
import { hot } from 'react-hot-loader';
import { BrowserRouter, Routes, Route, Navigate, Outlet } from 'react-router-dom';

// main redux store
import { store } from 'app/store';
import { Provider } from 'react-redux';

// layouts
import PublicLayout from 'layouts/public';
import PrivateLayout from 'layouts/private';

// core pages
import routes from 'routes';
import SignLists from 'pages/public/sign-lists';
import SignView from 'pages/public/sign-view';
import SignIn from 'pages/auth/sign-in';
// maintain pages
import DebugView from 'pages/public/debug';
import NotFound from 'pages/404';

// authentication routes
function AuthRoutes() {
  return (
    <PublicLayout background='white'>
      <Outlet />
    </PublicLayout>
  );
}

// protected routes
function PrivateRoutes({ menu, redirectPath = '/auth/sign-in' }) {
  // const refreshTokenValid = Cookies.get('accessToken');
  const user = sessionStorage.getItem('_user_info');

  return user ? (
    <PrivateLayout navMenu={menu}>
      <Outlet />
    </PrivateLayout>
  ) : (
    <Navigate to={redirectPath} replace />
  );
}

PrivateRoutes.propTypes = {
  menu: PropTypes.array,
  redirectPath: PropTypes.string,
};

// public routes
function PublicRoutes() {
  return (
    <PublicLayout>
      <Outlet />
    </PublicLayout>
  );
}

// get private routes from JSON
const getRoutes = (allRoutes) =>
  allRoutes.map((route) => {
    if (route.collapse) {
      return getRoutes(route.collapse);
    }

    if (route.route) {
      if (route.route === '/')
        return <Route index path={route.route} element={route.component} key={route.key} />;
      return <Route path={route.route} element={route.component} key={route.key} />;
    }

    return null;
  });

// main app export
function App() {
  return (
    <Provider store={store}>
      <Suspense fallback={<div>Loading..</div>}>
        <BrowserRouter>
          <Routes>
            <Route path='auth' element={<AuthRoutes />}>
              <Route index path='sign-in' element={<SignIn />} />
            </Route>
            <Route path='public' element={<PublicRoutes />}>
              <Route index path='table' element={<SignLists />} />
              <Route
                path='table/view/:stationName&id=:stationID&lat=:lat&lon=:lon'
                element={<SignView />}
              />
              <Route
                path='table/view/debug/:stationName&id=:stationID&lat=:lat&lon=:lon'
                element={<DebugView />}
              />
            </Route>
            <Route path='/' element={<PrivateRoutes menu={routes} />}>
              {getRoutes(routes)}
            </Route>
            <Route path='*' element={<NotFound />} />
          </Routes>
        </BrowserRouter>
      </Suspense>
    </Provider>
  );
}

if (module.hot) {
  module.hot.dispose(() => (window.__INITIAL_STATE__ = store.getState()));
}

// set environment before export
export default process.env.NODE_ENV === 'development' ? hot(module)(App) : App;
