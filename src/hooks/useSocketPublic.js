/* eslint-disable no-unused-vars */
import { useState, useEffect, useCallback } from 'react';
import { io } from 'socket.io-client';

import { isEmpty } from 'utils';

import { store } from 'app/store';
import map from 'app/reducers/map';
import sign from 'app/reducers/sign';

const { setPublicVehiclesEmit, setPublicVehiclesIn } = map.actions;
const { setReload } = sign.actions;

const MAP_SOCKET_SERVER_URL = 'https://api.mappico.co.th';
const MAP_SOCKET_PATH = process.env.REACT_APP_SOCKET_PATH;
const MAP_SOCKET_EMIT = process.env.REACT_APP_SOCKET_EMIT;
const MAP_SOCKET_DATA = process.env.REACT_APP_SOCKET_DATA;
const MAP_SOCKET_EVENT = process.env.REACT_APP_SOCKET_EVENT;

const VEH_SOCKET_SERVER_URL = process.env.REACT_APP_SOCKET_SIGN_URL;
const VEH_SOCKET_PATH = process.env.REACT_APP_SOCKET_SIGN_PATH;
const VEH_SOCKET_EMIT = process.env.REACT_APP_SOCKET_SIGN_EMIT;
const VEH_SOCKET_DATA = process.env.REACT_APP_SOCKET_SIGN_DATA;
const VEH_SOCKET_EVENT = process.env.REACT_APP_SOCKET_SIGN_EVENT;

const useSocketPublic = (id) => {
  const [mapSocket, setMapSocket] = useState();
  const [alertSocket, setAlertSocket] = useState();
  const [navViewSocket, setNavViewSocket] = useState();

  useEffect(() => {
    if (mapSocket === undefined) {
      const clientVehicle = io(MAP_SOCKET_SERVER_URL, {
        path: MAP_SOCKET_PATH,
      });

      setMapSocket(clientVehicle);

      clientVehicle.on('connect', () => console.log('vehicles socket connected'));
    }

    if (alertSocket === undefined) {
      const clientLine = io(VEH_SOCKET_SERVER_URL, {
        path: VEH_SOCKET_PATH,
      });

      setAlertSocket(clientLine);

      clientLine.on('connect', () => console.log('bus line socket connected'));
    }

    if (navViewSocket === undefined) {
      const clientView = io('https://tuarai.mappico.co.th', {
        path: '/socket.io',
      });

      setNavViewSocket(clientView);

      clientView.on('connect', () => console.log('boss socket connected'));
    }
  }, [alertSocket, mapSocket, navViewSocket]);

  const handleEmitData = useCallback((data) => {
    const { map } = store.getState();
    let temp = [...map.public_vehicles_emit];

    let _emitter = temp.map((v) => {
      if (v.tracker_id === data.id) return { ...v, obd_data: data, status: data.carstatus };

      return v;
    });

    store.dispatch(setPublicVehiclesEmit(_emitter));
  }, []);

  const handleSignalData = useCallback((data) => {
    const { map } = store.getState();
    let vehicle_list = [...map.public_vehicles_emit];

    const parse = Object.entries(JSON.parse(data)).map((e) => e[1]);

    let _filter = parse.map((obj) => ({
      ...vehicle_list.find((v) => v.tracker_id === obj.cid && v),
      ...{ time_left: obj.time_in_minute, bl_id: obj.bl_id },
    }));

    store.dispatch(setPublicVehiclesIn(_filter));
  }, []);

  const handleNavViewChange = useCallback((data) => {
    store.dispatch(setReload('RELOAD'));
  }, []);

  const subscribeMapChannel = useCallback(
    (socket) => {
      socket.emit(MAP_SOCKET_EMIT, MAP_SOCKET_DATA);
      socket.on(MAP_SOCKET_EVENT, handleEmitData);
    },
    [handleEmitData]
  );

  const subscribeAlertChannel = useCallback(
    (socket) => {
      socket.emit(VEH_SOCKET_EMIT, VEH_SOCKET_DATA);
      socket.on(`${VEH_SOCKET_EVENT}${id}`, handleSignalData);
    },
    [handleSignalData, id]
  );

  const subscribeNavView = useCallback(
    (socket) => {
      socket.emit('message', 'reload');
      socket.on('message', handleNavViewChange);
    },
    [handleNavViewChange]
  );

  useEffect(() => {
    if (!isEmpty(mapSocket)) subscribeMapChannel(mapSocket);
    if (!isEmpty(alertSocket)) subscribeAlertChannel(alertSocket);
    if (!isEmpty(navViewSocket)) subscribeNavView(navViewSocket);
  }, [
    alertSocket,
    subscribeMapChannel,
    mapSocket,
    subscribeAlertChannel,
    navViewSocket,
    subscribeNavView,
  ]);
};

export default useSocketPublic;
