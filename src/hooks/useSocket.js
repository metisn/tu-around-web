import { useState, useEffect, useCallback } from 'react';
import { io } from 'socket.io-client';

import { isEmpty } from 'utils';

import { store } from 'app/store';
import map from 'app/reducers/map';
const { setVehiclesEmit } = map.actions;

const SOCKET_SERVER_URL = 'https://api.mappico.co.th';
const SOCKET_PATH = process.env.REACT_APP_SOCKET_PATH;
const SOCKET_EMIT = process.env.REACT_APP_SOCKET_EMIT;
const SOCKET_DATA = process.env.REACT_APP_SOCKET_DATA;
const SOCKET_EVENT = process.env.REACT_APP_SOCKET_EVENT;

const useSocket = () => {
  const [socket, setSocket] = useState();

  useEffect(() => {
    if (socket === undefined) {
      const client = io(SOCKET_SERVER_URL, {
        path: SOCKET_PATH,
      });

      setSocket(client);

      client.on('connect', () => console.log('socket connected'));
    }
  }, [socket]);

  const handleEmitData = useCallback((data) => {
    const { map } = store.getState();
    let temp = [...map.vehicles_emit];

    let _emitter = temp.map((v) => {
      if (v.tracker_id === data.id) return { ...v, obd_data: data, status: data.carstatus };

      return v;
    });

    store.dispatch(setVehiclesEmit(_emitter));
  }, []);

  const subscribeChannel = useCallback(
    (socket) => {
      socket.emit(SOCKET_EMIT, SOCKET_DATA);
      socket.on(SOCKET_EVENT, handleEmitData);
    },
    [handleEmitData]
  );

  useEffect(() => {
    if (!isEmpty(socket)) subscribeChannel(socket);
  }, [socket, subscribeChannel]);
};

export default useSocket;
