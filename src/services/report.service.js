import { requestHandler, actionHandler } from 'middleware';

import { isEmpty } from 'utils';
// reducer actions
import report from 'app/reducers/report';
const { setConsumption } = report.actions;

const getConsumption = ({ start, end, id }) => {
  const callAction = async (dispatch) => {
    const response = await requestHandler({
      useMock: false,
      method: 'GET',
      url: `https://api.mappico.co.th/api/v2/thammasat/trip/consumption?from=${start}&to=${end}&device_id=${id}`,
      delay: 1,
    });

    if (!isEmpty(response)) dispatch(setConsumption(response));

    return response;
  };

  return actionHandler({
    callAction,
  });
};

export default {
  getConsumption,
};
