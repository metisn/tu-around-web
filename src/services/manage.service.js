/* eslint-disable no-unused-vars */
import Cookies from 'js-cookie';
import { requestHandler, actionHandler } from 'middleware';

let host = process.env.REACT_APP_API_URL || '';

const getAllVehicles = () => {
  const callAction = async () => {
    const accessToken = Cookies.get('accessToken');

    const { data } = await requestHandler({
      headers: { Authorization: `Bearer ${accessToken}` },
      useMock: false,
      method: 'GET',
      url: `${host}/api/data/vehicle`,
      delay: 1,
    });

    return data;
  };

  return actionHandler({
    callAction,
  });
};

const postUpdateVehicle = (body) => {
  const callAction = async () => {
    const accessToken = Cookies.get('accessToken');

    const { status } = await requestHandler({
      headers: { Authorization: `Bearer ${accessToken}` },
      useMock: false,
      method: 'POST',
      url: `${host}/api/update/vehicle`,
      data: body,
      delay: 1,
    });

    return status;
  };

  return actionHandler({
    loadingMessage: 'Loading...',
    successMessage: 'Update complete',
    callAction,
  });
};

const postUploadImage = (body) => {
  const callAction = async () => {
    const accessToken = Cookies.get('accessToken');

    const { data } = await requestHandler({
      headers: { Authorization: `Bearer ${accessToken}` },
      useMock: false,
      method: 'POST',
      url: `${host}/upload/image`,
      data: body,
      delay: 1,
    });

    return data;
  };

  return actionHandler({
    loadingMessage: 'Loading...',
    successMessage: 'Upload complete',
    callAction,
  });
};

export default {
  getAllVehicles,
  postUpdateVehicle,
  postUploadImage,
};
