/* eslint-disable no-unused-vars */
import Cookies from 'js-cookie';
import { requestHandler, actionHandler } from 'middleware';

import sign from 'app/reducers/sign';
const { setMedias, setNavView } = sign.actions;

let host = process.env.REACT_APP_API_URL || '';

// import open_weather API
let weather_url = process.env.REACT_APP_OPEN_WEATHER_URL || '';
let weather_key = process.env.REACT_APP_OPEN_WEATHER_KEY || '';

const getStationList = () => {
  const callAction = async () => {
    const { data } = await requestHandler({
      useMock: false,
      method: 'GET',
      url: `${host}/api/data/station`,
      delay: 1,
    });

    return data;
  };

  return actionHandler({
    callAction,
  });
};

const getSignMedias = () => {
  const callAction = async (dispatch) => {
    const accessToken = Cookies.get('accessToken');

    const { data, status } = await requestHandler({
      headers: { Authorization: `Bearer ${accessToken}` },
      useMock: false,
      method: 'GET',
      url: `${host}/api/data/media`,
      delay: 1,
    });

    if (status) {
      dispatch(setMedias(data.results));
    }

    return data;
  };

  return actionHandler({
    callAction,
  });
};

const postSignMedias = (body) => {
  const callAction = async () => {
    const accessToken = Cookies.get('accessToken');

    const { status } = await requestHandler({
      headers: { Authorization: `Bearer ${accessToken}`, 'Content-Type': 'multipart/form-data' },
      useMock: false,
      method: 'POST',
      url: `${host}/upload/media`,
      data: body,
      delay: 1,
    });

    return status;
  };

  return actionHandler({
    loadingMessage: 'Loading...',
    successMessage: 'Upload media success',
    callAction,
  });
};

const deleteSignMedias = ({ id }) => {
  const callAction = async () => {
    const accessToken = Cookies.get('accessToken');

    const { status } = await requestHandler({
      headers: { Authorization: `Bearer ${accessToken}` },
      useMock: false,
      method: 'DELETE',
      url: `${host}/api/data/media/${id}`,
      delay: 1,
    });

    return status;
  };

  return actionHandler({
    loadingMessage: 'Loading...',
    successMessage: 'Delete media success',
    callAction,
  });
};

const getNavView = () => {
  const callAction = async (dispatch) => {
    const accessToken = Cookies.get('accessToken');

    const { data, status } = await requestHandler({
      headers: { Authorization: `Bearer ${accessToken}` },
      useMock: false,
      method: 'GET',
      url: `${host}/api/data/navview`,
      delay: 1,
    });

    if (status) {
      dispatch(setNavView(data.results));
    }

    return data;
  };

  return actionHandler({
    callAction,
  });
};

const postUpdateNavView = (body) => {
  const callAction = async () => {
    const accessToken = Cookies.get('accessToken');

    const { status } = await requestHandler({
      headers: { Authorization: `Bearer ${accessToken}` },
      useMock: false,
      method: 'POST',
      url: `${host}/api/update/navview`,
      data: body,
      delay: 1,
    });

    return status;
  };

  return actionHandler({
    loadingMessage: 'Loading...',
    successMessage: 'Update success',
    callAction,
  });
};

const postUpdateEmergencyView = (body) => {
  const callAction = async () => {
    const accessToken = Cookies.get('accessToken');

    const { status } = await requestHandler({
      headers: { Authorization: `Bearer ${accessToken}` },
      useMock: false,
      method: 'POST',
      url: `${host}/api/update/emsview`,
      data: body,
      delay: 1,
    });

    return status;
  };

  return actionHandler({
    loadingMessage: 'Loading...',
    successMessage: 'Update success',
    callAction,
  });
};

export default {
  getStationList,
  getSignMedias,
  postSignMedias,
  deleteSignMedias,
  getNavView,
  postUpdateNavView,
  postUpdateEmergencyView,
};
