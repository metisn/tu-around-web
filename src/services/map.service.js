import { requestHandler, actionHandler } from 'middleware';
import Cookies from 'js-cookie';

// reducer actions
import map from 'app/reducers/map';
const { setBusLine } = map.actions;

let host = process.env.REACT_APP_API_URL || '';

const getMapLine = () => {
  const callAction = async (dispatch) => {
    const { data, success } = await requestHandler({
      useMock: true,
      jsonMock: 'map-routes.json',
      method: 'POST',
      url: `${host}/get`,
      delay: 0,
    });

    if (success) {
      dispatch(setBusLine(data));
    }

    return data;
  };

  return actionHandler({
    callAction,
  });
};

const getMapStop = () => {
  const callAction = async () => {
    const response = await requestHandler({
      useMock: true,
      jsonMock: 'map-stops.json',
      method: 'POST',
      url: `${host}/get`,
      delay: 0,
    });

    return response;
  };

  return actionHandler({
    callAction,
  });
};

const getMappicoVehicleLists = () => {
  const callAction = async () => {
    const response = await requestHandler({
      useMock: false,
      method: 'GET',
      url: `https://api.mappico.co.th/api/v2/thammasat/vehicles/list`,
      delay: 0,
    });

    return response;
  };

  return actionHandler({
    callAction,
  });
};

const getAllVehicles = () => {
  const callAction = async () => {
    const accessToken = Cookies.get('accessToken');

    const { data } = await requestHandler({
      headers: { Authorization: `Bearer ${accessToken}` },
      useMock: false,
      method: 'GET',
      url: `${host}/api/data/alltracker`,
      delay: 0,
    });

    return data;
  };

  return actionHandler({
    callAction,
  });
};

export default {
  getMapLine,
  getMapStop,
  getMappicoVehicleLists,
  getAllVehicles,
};
