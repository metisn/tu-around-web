import { requestHandler, actionHandler } from 'middleware';
import Cookies from 'js-cookie';

import auth from 'app/reducers/auth';
const { loginSuccess, logoutSuccess } = auth.actions;

let host = process.env.REACT_APP_API_URL || '';

const requestLogin = ({ email, password }, redirect) => {
  const callAction = async (dispatch) => {
    const { data, status } = await requestHandler({
      useMock: false,
      jsonMock: 'auth-login.json',
      method: 'POST',
      url: `${host}/api/auth/login`,
      data: { username: email, password },
      delay: 1,
    });

    if (status) {
      Cookies.set('accessToken', data.accessToken);
      sessionStorage.setItem('_user_info', JSON.stringify({ email: data.email, name: data.name }));

      dispatch(loginSuccess({ email: data.email, name: data.name }));
    }

    return data;
  };

  return actionHandler({
    loadingMessage: 'Waiting for request...',
    successMessage: 'Login success',
    callAction,
    callRedirect: () => redirect(),
  });
};

const requestLogout = (redirect) => {
  const callAction = async (dispatch) => {
    const { status } = await requestHandler({
      method: 'POST',
      url: `${host}/api/auth/logout`,
      delay: 1,
    });

    if (status) {
      sessionStorage.clear();

      Cookies.remove('accessToken');
      dispatch(logoutSuccess());
    }

    return true;
  };

  return actionHandler({
    loadingMessage: 'Cleaning session...',
    successMessage: 'Logout success',
    callAction,
    callRedirect: () => redirect(),
  });
};

export default {
  requestLogin,
  requestLogout,
};
