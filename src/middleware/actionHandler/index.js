import { toast } from 'react-toastify';
import { isEmpty } from 'utils';

import { toastOptions } from 'utils/constants/toast';
import { v4 as uuidv4 } from 'uuid';

const actionHandler =
  ({ toastId, loadingMessage, successMessage, callAction, callRedirect }) =>
  async (dispatch, getState) => {
    let res = {};
    let loading = toastId ? toastId : uuidv4();

    try {
      if (loadingMessage) {
        toast.loading(loadingMessage, { ...toastOptions, toastId: loading });
      }

      res = await callAction(dispatch, getState);

      if (!isEmpty(res)) {
        if (successMessage) {
          toast.update(loading, {
            render: successMessage,
            type: toast.TYPE.SUCCESS,
            isLoading: false,
            autoClose: true,
            onClose: callRedirect ? callRedirect() : null,
            theme: 'dark',
            style: {
              color: 'white',
              background: '#07bc0c',
            },
            bodyClassName: 'override-icon-class',
            progressClassName: 'override-progress-class',
          });
        }
        if (loadingMessage && !successMessage) toast.dismiss(loading);

        if (callRedirect && !successMessage) callRedirect();
      } else {
        if (loadingMessage) {
          toast.dismiss(loading);
        }
      }

      return res;
    } catch (error) {
      if (loadingMessage) {
        toast.update(loading, {
          render: error.message,
          type: error.type?.toLocaleLowerCase() || toast.TYPE.ERROR,
          isLoading: false,
          autoClose: 5000,
          theme: 'dark',
          style: {
            color: 'white',
            background:
              error.type?.toLocaleLowerCase() === toast.TYPE.ERROR ? '#e74c3c' : '#f1c40f',
          },
          bodyClassName: 'override-icon-class',
          progressClassName: 'override-progress-class',
        });
      } else {
        toast.error(error.message, {
          ...toastOptions,
          toastId: loading,
          autoClose: 5000,
          theme: 'colored',
        });
      }
    }
  };

export default actionHandler;
