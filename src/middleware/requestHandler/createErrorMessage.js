const errorMessage = (res) => {
  if (res.status === 200 && !res.data.success) return 'Error message 200';
  if (res.status === 400) return 'Error message 400';
  if (res.status === 401) return 'Error 401, Access Denied';
  if (res.status === 403) return 'Error message 403';
  if (res.status === 404) return 'Error message 404';
  if (res.status === 500) return `Internal Server Error`;

  return res.statusText;
};

const getMessage = (res) => {
  if (res === undefined) return 'Network request timed out';
  // to do: change to i18n later
  if (res.data?.message) return res.data.message;
  if (res.head) return res.head.message;
  if (res.status) return errorMessage(res);
  return res.statusText;
};

const getType = (res) => {
  if (!res) return 'ERROR';
  if (res.data?.message) return res.data.message.type;
  return 'ERROR';
};

const createErrorMessage = (res, options) => {
  throw Object({
    type: getType(res) || 'ERROR',
    message: getMessage(res, options),
  });
};

const getErrorMessage = (res) => {
  if (res.response) {
    const { data } = res.response;
    return {
      error: {
        type: 'ERROR',
        status: res.status,
        statusText: errorMessage(res),
        message: data.message,
        success: data.success,
      },
    };
  } else {
    return {
      error: {
        type: 'ERROR',
        code: res.code,
        res: res,
      },
    };
  }
};

export { createErrorMessage, getErrorMessage };
