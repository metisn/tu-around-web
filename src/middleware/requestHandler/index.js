import axios from 'axios';

import { createErrorMessage, getErrorMessage } from './createErrorMessage';
import isServiceError from './isServiceError';

const defaultOptions = {
  useMock: false,
  delay: 0,
  jsonMock: '',
  method: 'get',
  url: '',
  params: {},
  data: {},
  timeout: 30000,
  headers: {},
  fileName: null,
};

const requestHandler = (userOptions) => {
  const options = { ...defaultOptions, ...userOptions };
  const { useMock, jsonMock, url, delay } = options;
  const serviceURL = useMock ? `/mock/${jsonMock}` : url;

  return new Promise((resolve) => {
    setTimeout(async () => {
      if (useMock) {
        options.method = 'get';
        options.url = serviceURL;
      }

      resolve(fetch(options));
    }, delay * 1000);
  });
};

const fetch = async (options) => {
  let res;

  try {
    res = await axios(options);

    if (isServiceError(res, options)) {
      createErrorMessage(res, options);
    }
  } catch (error) {
    if (res === undefined && isServiceError(error.response, options)) {
      if (options.ssr) return getErrorMessage(error, options);

      createErrorMessage(error.response, options);
    }
  }

  return res.data;
};

export default requestHandler;
