/* eslint-disable react/prop-types */
import { memo, useEffect, useRef, useState } from 'react';
import { Marker, Popup } from 'react-leaflet';

const CustomMarker = memo(({ leaflet, isActive, data, map, icon }) => {
  const [refReady, setRefReady] = useState(false);
  let popupRef = useRef();

  useEffect(() => {
    if (refReady && isActive) {
      popupRef.openOn(map);
    }
  }, [isActive, refReady, map]);

  const myIcon = leaflet.icon({
    iconUrl: icon,
    iconSize: [24, 24],
    iconAnchor: [12, 12],
    popupAnchor: [0, -12],
  });

  return (
    <Marker icon={myIcon} position={data.position}>
      <Popup
        ref={(r) => {
          popupRef = r;
          setRefReady(true);
        }}
      >
        คุณอยู่ที่นี่ <br></br>
      </Popup>
    </Marker>
  );
});

CustomMarker.displayName = 'CustomMarker';

export default CustomMarker;
