/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import { memo, forwardRef, useRef, useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import {
  MapContainer,
  TileLayer,
  GeoJSON,
  LayersControl,
  Marker,
  Popup,
  Tooltip,
  LayerGroup,
} from 'react-leaflet';
import { FullscreenControl } from 'react-leaflet-fullscreen';
import 'react-leaflet-fullscreen/dist/styles.css';
import './override.css';
import 'leaflet-rotatedmarker';

import CustomMarker from './CustomMarker';

import busIconNormal from 'assets/icons/ngv_normal.png';
import busIconRed from 'assets/icons/ngv_red.png';
import busIconYellow from 'assets/icons/ngv_yellow.png';
import busIconGreen from 'assets/icons/ngv_green.png';
import busIconPurple from 'assets/icons/ngv_purple.png';
import busIconBlue from 'assets/icons/ngv_blue.png';
import busStopIcon from 'assets/icons/bus_stop.png';

import { isEmpty } from 'utils';

import L from 'leaflet';

// eslint-disable-next-line no-unused-vars
const MapComponent = ({
  center,
  zoom,
  bounds,
  geoJson,
  marker,
  vehicles,
  height,
  fullScreen,
  draggable,
  currentLocation,
  hideLayer,
}) => {
  const [map, setMap] = useState();
  // eslint-disable-next-line react/display-name
  const RotatedMarker = forwardRef(({ children, ...props }, forwardRef) => {
    const markerRef = useRef();

    const { rotationAngle, rotationOrigin } = props;

    useEffect(() => {
      const marker = markerRef.current;
      if (marker) {
        marker.setRotationAngle(rotationAngle);
        marker.setRotationOrigin(rotationOrigin);
      }
    }, [rotationAngle, rotationOrigin]);

    return (
      <Marker
        ref={(ref) => {
          markerRef.current = ref;
          if (forwardRef) {
            forwardRef.current = ref;
          }
        }}
        {...props}
      >
        {children}
      </Marker>
    );
  });

  function getName({ name }) {
    switch (name) {
      case 'red':
        return 'สาย สีแดง';
      case 'yellow':
        return 'สาย สีเหลือง';
      case 'green':
        return 'สาย สีเขียว';
      case 'purple':
        return 'สาย สีม่วง';
      case 'blue':
        return 'สาย สีฟ้า';
      default:
        return 'เส้นถนน';
    }
  }

  function toggleHideLayer() {
    document.getElementsByClassName('leaflet-control-attribution')[0].style.display = 'none';
    document.getElementsByClassName('leaflet-bottom leaflet-right')[0].style.display = 'none';
  }

  return (
    <MapContainer
      id='mapId'
      bounds={bounds}
      center={center}
      zoom={zoom}
      style={{ width: '100%', height: height }}
      dragging={draggable}
      zoomControl={draggable}
      doubleClickZoom={draggable}
      scrollWheelZoom={draggable}
      whenReady={() => hideLayer && toggleHideLayer()}
      whenCreated={setMap}
    >
      <TileLayer
        attribution='<a href="http://jawg.io" title="Tiles Courtesy of Jawg Maps" target="_blank">&copy; <b>Jawg</b>Maps</a> &copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url='https://{s}.tile.jawg.io/jawg-sunny/{z}/{x}/{y}{r}.png?access-token=Dok07MSzv5dobtqw0P8MoyAdPVNs3GMxX2ChpuQEq3HoOlm1NyntHbPP8UqFhuIp'
      />

      {fullScreen && <FullscreenControl forceSeparateButton position='topright' />}

      {vehicles &&
        vehicles.map((item, index) => {
          const switchIcon = ({ bus_line }) => {
            switch (bus_line) {
              case 'red':
                return busIconRed;
              case 'yellow':
                return busIconYellow;
              case 'green':
                return busIconGreen;
              case 'purple':
                return busIconPurple;
              case 'blue':
                return busIconBlue;
              default:
                return busIconNormal;
            }
          };

          const myIcon = L.icon({
            iconUrl: switchIcon(item),
            iconSize: [24, 48],
          });

          if (isEmpty(item.obd_data)) return null;

          return (
            <RotatedMarker
              key={item.tracker_id + index}
              position={[item.obd_data.lat, item.obd_data.lon]}
              rotationAngle={item.obd_data.direction}
              rotationOrigin='center'
              icon={myIcon}
            >
              <Tooltip sticky opacity={1}>
                {item.tracker_id}
              </Tooltip>
            </RotatedMarker>
          );
        })}

      <LayersControl position='bottomright'>
        {geoJson &&
          geoJson.map((items, index) => (
            <LayersControl.Overlay
              key={index}
              checked={items.name !== 'gray' ? true : false}
              name={getName(items)}
            >
              <GeoJSON
                key='my-geojson'
                data={items.features}
                style={{
                  color: items.name,
                  opacity: '0.75',
                }}
              />
            </LayersControl.Overlay>
          ))}

        {marker && (
          <LayersControl.Overlay checked name={marker.name}>
            <LayerGroup>
              {marker?.features?.map((item, index) => {
                const swapCoordinates = (coordinates) => [coordinates[1], coordinates[0]];

                const myIcon = L.icon({
                  iconUrl: busStopIcon,
                  iconSize: [24, 24],
                  iconAnchor: [12, 12],
                  popupAnchor: [0, -12],
                });

                return (
                  <Marker
                    key={index}
                    icon={myIcon}
                    position={swapCoordinates(item.geometry.coordinates)}
                  >
                    <Popup>{item.properties.name_th}</Popup>
                  </Marker>
                );
              })}
            </LayerGroup>
          </LayersControl.Overlay>
        )}
      </LayersControl>

      {currentLocation && (
        <CustomMarker
          leaflet={L}
          map={map}
          data={{
            position: [currentLocation.coordinate[0], currentLocation.coordinate[1]],
          }}
          isActive
          icon={busStopIcon}
        />
      )}
    </MapContainer>
  );
};

MapComponent.propTypes = {
  center: PropTypes.array,
  zoom: PropTypes.number,
  bounds: PropTypes.array,
  geoJson: PropTypes.array,
  marker: PropTypes.object,
  vehicles: PropTypes.array,
  loading: PropTypes.bool,
  height: PropTypes.string,
  fullScreen: PropTypes.bool,
  draggable: PropTypes.bool,
  currentLocation: PropTypes.object,
  hideLayer: PropTypes.bool,
};

MapComponent.defaultProps = {
  center: [14.0713, 100.6062],
  bounds: [],
  geoJson: [],
  marker: {},
  vehicles: [],
  loading: false,
  height: 'calc(100vh - 121px)',
  fullScreen: true,
  draggable: true,
  hideLayer: false,
};

export default memo(MapComponent);
