/* eslint-disable no-unused-vars */
import { useState, useEffect, useCallback } from 'react';
import DataTable from 'components/Table';
import Card from '@mui/material/Card';
import Box from 'components/Box';
import Button from 'components/Button';
import Input from 'components/Input';
import endOfMonth from 'date-fns/endOfMonth';
import { format } from 'date-fns';
import TableCell from '@mui/material/TableCell';
import { Stack, Typography } from '@mui/material';
import { Icon } from '@mui/material';
import Autocomplete from '@mui/material/Autocomplete';
import { Grid } from '@mui/material';
import SearchIcon from '@mui/icons-material/Search';
import FormField from 'components/FormField';
import ReportModal from './modal';
import { useDispatch } from 'react-redux';
import startOfMonth from 'date-fns/startOfMonth';
import { AdapterDateFns } from '@mui/x-date-pickers/AdapterDateFns';
import { LocalizationProvider } from '@mui/x-date-pickers/LocalizationProvider';
import { DatePicker } from '@mui/x-date-pickers/DatePicker';
import mapService from 'services/map.service';
import reportService from 'services/report.service';
import manageService from 'services/manage.service';
import { useStateLoading } from 'hooks';
import { useSelector } from 'react-redux';
import { isEmpty } from 'utils';
import InputAdornment from '@mui/material/InputAdornment';
const Report = () => {
  const dispatch = useDispatch();
  const [month, setMonth] = useState(new Date());
  const [bodyData, setBodyData] = useState([]);
  const [idList, setIdList] = useState([]);
  const [selectedId, setSelectedId] = useState({});
  const [open, setModal] = useState(false);
  const [selectedData, setSelectedData] = useState(null);

  const [vehicles, setVehicles] = useState([]);

  const { vehicle_lists } = useSelector((state) => state.manage);

  // loader
  const [fetching, fetchApi] = useStateLoading(['gettingVehicles', 'gettingReport']);

  const { getMappicoVehicleLists } = mapService;
  const { getConsumption } = reportService;
  const { getAllVehicles } = manageService;

  const callVehicleIds = useCallback(
    async () =>
      fetchApi('gettingVehicles', async () => {
        let { results } = await dispatch(getAllVehicles());

        if (isEmpty(vehicle_lists) && !isEmpty(results)) {
          let { data } = await dispatch(getMappicoVehicleLists());

          let filterVehicle = results.map((i) => {
            let filter = data.filter((obj) => obj.device_id == i.tracker_id);

            return {
              ...i,
              ...{
                status: filter[0]?.status || 'offline',
                latest_online: filter[0]?.latest_online || null,
                latest_address: filter[0]?.latest_address || null,
              },
            };
          });

          setVehicles(filterVehicle);
        }

        setIdList(results);
      }),
    [dispatch, fetchApi, getAllVehicles, getMappicoVehicleLists, vehicle_lists]
  );

  const callReport = useCallback(
    async (body) =>
      fetchApi('gettingReport', async () => {
        let res = await dispatch(getConsumption(body));

        if (res.details.length > 0) {
          let findId = await vehicles.filter((v) => v.tracker_id == selectedId);
          setBodyData([{ ...findId[0], ...res }]);
        } else {
          setBodyData([]);
        }
      }),
    [dispatch, fetchApi, getConsumption, selectedId, vehicles]
  );

  useEffect(() => {
    callVehicleIds();
  }, [callVehicleIds]);

  const handleMonth = (event) => {
    setMonth(event);
  };

  function toggleModalData(data) {
    setSelectedData(data);
    setModal(true);
  }

  // table info
  const headCells = [
    { id: 'bus_name', label: 'ชื่อรถ', align: 'left' },
    { id: 'driver_name', label: 'ชื่อคนขับ', align: 'left' },
    { id: 'device_type', label: 'ใช้งานทั้งหมด', align: 'left' },
    { id: 'sdfsdf', label: 'ระยะทาง/เดือน', align: 'left' },
    { id: 'sdffssdew', label: 'รอบวิ่ง/เดือน', align: 'left' },
    { id: 'href', label: '', align: 'right' },
  ];

  const bodyItems = ({ data, index, callAction }) => {
    function upperFirstCharacter(text) {
      return text?.charAt(0).toUpperCase() + text?.slice(1);
    }

    return (
      <>
        <TableCell component='th' scope='row'>
          {data.bus_name}
        </TableCell>
        <TableCell component='th' scope='row'>
          {upperFirstCharacter(data.driver_fname) + ' ' + upperFirstCharacter(data.driver_lname)}
        </TableCell>
        <TableCell>{data.details.length} วัน / เดือน</TableCell>
        <TableCell>{(data.summary.mileage * 1.6).toFixed(2)} km / เดือน</TableCell>
        <TableCell>{data.summary.trip} รอบ / เดือน</TableCell>
        <TableCell align='right'>
          <Button
            variant='contained'
            color='success'
            startIcon={<Icon>descriptionn</Icon>}
            onClick={() => callAction(data)}
          >
            ดูข้อมูลทั้งหมด
          </Button>
        </TableCell>
      </>
    );
  };

  const searchData = () => {
    const start = format(startOfMonth(new Date(month)), "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
    const end = format(endOfMonth(new Date(month)), "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    callReport({ start, end, id: selectedId });
  };

  return (
    <LocalizationProvider dateAdapter={AdapterDateFns}>
      <Box py={2}>
        <Grid container spacing={2} flexDirection='column'>
          <Grid item xs={12}>
            <Card>
              <Stack p={3} spacing={2} direction='row' sx={{ maxHeight: 92 }}>
                <DatePicker
                  label='Select Month'
                  views={['year', 'month']}
                  value={month}
                  onChange={(newValue) => {
                    handleMonth(newValue);
                  }}
                  orientation='portrait'
                  renderInput={(params) => <Input {...params} />}
                  cancelText={null}
                  okText={<Typography color='primary'>Confirm</Typography>}
                />
                <Autocomplete
                  loading
                  options={idList}
                  value={idList.find((obj) => obj.tracker_id == selectedId) || null}
                  isOptionEqualToValue={(option, value) => option.tracker_id == value.tracker_id}
                  getOptionLabel={(option) => option.bus_name}
                  onChange={(event, value) => setSelectedId(value?.tracker_id)}
                  renderInput={(params) => <FormField {...params} label='ชื่อรถ' />}
                  sx={{ width: '200px' }}
                />

                <Button
                  variant='gradient'
                  color='primary'
                  startIcon={<SearchIcon />}
                  onClick={searchData}
                >
                  ค้นหา
                </Button>
              </Stack>
            </Card>
          </Grid>
          <Grid item xs={12}>
            <Card sx={{ overflow: 'hidden' }}>
              <DataTable
                TableBodyItem={bodyItems}
                headCellsData={headCells}
                bodyCellsData={bodyData}
                handleAction={toggleModalData}
                isLoading={fetching.gettingReport}
                emptyText='ไม่พบข้อมูลการวิ่ง'
              />
            </Card>
          </Grid>
        </Grid>

        <ReportModal
          open={open}
          data={selectedData}
          month={month}
          onClose={() => setModal(false)}
        />
      </Box>
    </LocalizationProvider>
  );
};

export default Report;
