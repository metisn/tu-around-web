import { memo } from 'react';
import PropTypes from 'prop-types';

import { Grid, Stack } from '@mui/material';
import Modal from '@mui/material/Modal';
import Fade from '@mui/material/Fade';
import Card from '@mui/material/Card';

import Button from 'components/Button';

import { format } from 'date-fns';
import Icon from '@mui/material/Icon';
import endOfMonth from 'date-fns/endOfMonth';
// import Typography from 'components/Typography';

import Box from 'components/Box';
import Typography from 'components/Typography';
import { isEmpty } from 'utils';

const style = {
  width: '100%',
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  display: 'flex',
  justifyContent: 'center',
};

const DriverDetail = ({ data }) => {
  function upperFirstCharacter(text) {
    return text?.charAt(0).toUpperCase() + text?.slice(1);
  }

  function getColor({ bus_line }) {
    switch (bus_line) {
      case 'yellow':
        return 'warning';
      case 'green':
        return 'success';
      case 'purple':
        return 'extra';
      case 'blue':
        return 'info';
      default:
        return 'primary';
    }
  }

  function getName({ bus_line }) {
    switch (bus_line) {
      case 'yellow':
        return 'สาย 1B สีเหลือง';
      case 'green':
        return 'สาย 2 สีเขียว';
      case 'purple':
        return 'สาย 3 สีม่วง';
      case 'blue':
        return 'สาย 5 สีฟ้า';
      default:
        return 'สาย 1A สีแดง';
    }
  }

  return (
    <Box
      sx={{
        border: '1px solid rgba(0, 0, 0, 0.085)',
        maxWidth: '260px',
        maxHeight: '462px',
        borderRadius: '8px',
      }}
    >
      <Grid container>
        <Grid item xs={12}>
          {isEmpty(data.driver_img) ? (
            <div
              style={{
                width: '100%',
                height: 240,
                backgroundColor: 'lightgray',
                borderRadius: '8px 8px 0 0',

                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Icon color='disabled' fontSize='large'>
                image
              </Icon>
            </div>
          ) : (
            <img
              src={data.driver_img}
              alt='driver_img'
              style={{
                width: '100%',
                height: 240,
                backgroundColor: 'lightgray',
                borderRadius: '8px 8px 0 0',

                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            />
          )}
        </Grid>

        <Grid item xs={12} mt={2} mb={1}>
          <Stack direction='row' justifyContent='space-between' alignItems='center' m={1.5}>
            <Typography variant='subtitle2'>ชื่อคนขับ</Typography>
            <Typography variant='body2' sx={{ fontWeight: 400 }}>
              {upperFirstCharacter(data.driver_fname) +
                ' ' +
                upperFirstCharacter(data.driver_lname)}
            </Typography>
          </Stack>
          <Stack direction='row' justifyContent='space-between' alignItems='center' m={1.5}>
            <Typography variant='subtitle2'>เบอร์โทร</Typography>
            <Typography variant='body2' sx={{ fontWeight: 400 }}>
              {data.driver_phone}
            </Typography>
          </Stack>
          <Stack direction='row' justifyContent='space-between' alignItems='center' m={1.5}>
            <Typography variant='subtitle2'>สายรถ</Typography>
            <Typography variant='body2' color={getColor(data)} sx={{ fontWeight: 400 }}>
              {getName(data)}
            </Typography>
          </Stack>
          <Stack direction='row' justifyContent='space-between' alignItems='center' m={1.5}>
            <Typography variant='subtitle2'>ทะเบียนรถ</Typography>
            <Typography variant='body2' sx={{ fontWeight: 400 }}>
              {data.plate_alphabet + '-' + data.plate_number}
            </Typography>
          </Stack>
          <Stack direction='row' justifyContent='space-between' alignItems='center' m={1.5}>
            <Typography variant='subtitle2'>Tracker ID</Typography>
            <Typography variant='body2' sx={{ fontWeight: 400 }}>
              {data.tracker_id}
            </Typography>
          </Stack>
        </Grid>
      </Grid>
    </Box>
  );
};

DriverDetail.propTypes = {
  data: PropTypes.object,
};

const VehicleDetail = ({ data, month }) => {
  function calcAvgMilesUsage({ details }) {
    let cal = 0;
    details.map((t) => (cal += t.sum.trip_mileage * 1.6)); // miles to km

    return `${(cal / details.length).toFixed(2)} กิโลเมตร/วัน`;
  }

  function calcDayUsedPerMonth({ details }) {
    let day = details.length;
    const end = format(endOfMonth(new Date(month)), 'dd');

    return `${day}/${end} วัน`;
  }

  function calcAvgTripPerMonth({ summary }) {
    let trip = summary.trip;
    const dayInMonth = format(endOfMonth(new Date(month)), 'dd');

    return `${(trip / dayInMonth).toFixed(0)} ทริป/วัน`;
  }

  return (
    <Box
      sx={{
        border: '1px solid rgba(0, 0, 0, 0.085)',
        width: 'calc(100% - 270px)',
        height: '100%',
        borderRadius: '8px',
      }}
    >
      <Grid container flexDirection='row' sx={{ height: '100%' }}>
        <Grid item xs={12} md={6} sx={{ borderRight: '0.5px solid rgba(0, 0, 0, 0.085)' }} p={2}>
          <Typography variant='h5' color='primary' mb={3}>
            ผลการขับขี่โดยยรวม
          </Typography>

          <Box
            variant='gradient'
            bgColor='dark'
            color={'white'}
            width='100%'
            height='6rem'
            marginRight='auto'
            borderRadius='lg'
            display='flex'
            flexDirection='column'
            justifyContent='center'
            alignItems='start'
            shadow='md'
            pl={2}
          >
            <Typography variant='subtitle2' color='white'>
              ใช้งานทั้งหมด
            </Typography>
            <Typography variant='h4' color='white'>
              {data.details?.length} วัน
            </Typography>
          </Box>
          <Stack direction='row' justifyContent='space-between' alignItems='center' my={4}>
            <Box
              variant='gradient'
              bgColor='primary'
              color={'white'}
              width='3rem'
              height='3rem'
              marginRight='auto'
              borderRadius='lg'
              display='flex'
              justifyContent='center'
              alignItems='center'
              shadow='md'
            >
              <Icon fontSize='small'>route</Icon>
            </Box>
            <Stack alignItems='end' justifyContent='center'>
              <Typography variant='body1'>{`${(data.summary.mileage * 1.6).toFixed(
                2
              )} กิโลเมตร`}</Typography>
              <Typography variant='subtitle2' color='secondary'>
                ระยะทางทั้งหมด
              </Typography>
            </Stack>
          </Stack>
          <Stack direction='row' justifyContent='space-between' alignItems='center' my={4}>
            <Box
              variant='gradient'
              bgColor='primary'
              color={'white'}
              width='3rem'
              height='3rem'
              marginRight='auto'
              borderRadius='lg'
              display='flex'
              justifyContent='center'
              alignItems='center'
              shadow='md'
            >
              <Icon fontSize='small'>mode_of_travel</Icon>
            </Box>
            <Stack alignItems='end' justifyContent='center'>
              <Typography variant='body1'>{`${data.summary.trip} ทริป`}</Typography>
              <Typography variant='subtitle2' color='secondary'>
                จำนวนทริปทั้งหมด
              </Typography>
            </Stack>
          </Stack>
        </Grid>
        <Grid item xs={12} md={6} sx={{ borderLeft: '0.5px solid rgba(0, 0, 0, 0.085)' }} p={2}>
          <Typography variant='h5' color='primary' mb={2.5}>
            ผลการขับขี่เฉลี่ยต่อวัน
          </Typography>

          <Stack direction='row' justifyContent='space-between' alignItems='center' mb={4}>
            <Box
              variant='gradient'
              bgColor='secondary'
              color={'white'}
              width='3rem'
              height='3rem'
              marginRight='auto'
              borderRadius='lg'
              display='flex'
              justifyContent='center'
              alignItems='center'
              shadow='md'
            >
              <Icon fontSize='small'>access_time_filled_rounded</Icon>
            </Box>
            <Stack alignItems='end' justifyContent='center'>
              <Typography variant='body1'>null</Typography>
              <Typography variant='subtitle2' color='secondary'>
                เวลาที่ใช้ในการขับขี่
              </Typography>
            </Stack>
          </Stack>
          <Stack direction='row' justifyContent='space-between' alignItems='center' my={4}>
            <Box
              variant='gradient'
              bgColor='secondary'
              color={'white'}
              width='3rem'
              height='3rem'
              marginRight='auto'
              borderRadius='lg'
              display='flex'
              justifyContent='center'
              alignItems='center'
              shadow='md'
            >
              <Icon fontSize='small'>near_me_rounded</Icon>
            </Box>
            <Stack alignItems='end' justifyContent='center'>
              <Typography variant='body1'>{calcAvgMilesUsage(data)}</Typography>
              <Typography variant='subtitle2' color='secondary'>
                ระยะทาง
              </Typography>
            </Stack>
          </Stack>
          <Stack direction='row' justifyContent='space-between' alignItems='center' my={4}>
            <Box
              variant='gradient'
              bgColor='secondary'
              color={'white'}
              width='3rem'
              height='3rem'
              marginRight='auto'
              borderRadius='lg'
              display='flex'
              justifyContent='center'
              alignItems='center'
              shadow='md'
            >
              <Icon fontSize='small'>calendar_month</Icon>
            </Box>
            <Stack alignItems='end' justifyContent='center'>
              <Typography variant='body1'>{calcDayUsedPerMonth(data)}</Typography>
              <Typography variant='subtitle2' color='secondary'>
                วันเดินรถ
              </Typography>
            </Stack>
          </Stack>
          <Stack direction='row' justifyContent='space-between' alignItems='center' my={4}>
            <Box
              variant='gradient'
              bgColor='secondary'
              color={'white'}
              width='3rem'
              height='3rem'
              marginRight='auto'
              borderRadius='lg'
              display='flex'
              justifyContent='center'
              alignItems='center'
              shadow='md'
            >
              <Icon fontSize='small'>departure_board</Icon>
            </Box>
            <Stack alignItems='end' justifyContent='center'>
              <Typography variant='body1'>{calcAvgTripPerMonth(data)}</Typography>
              <Typography variant='subtitle2' color='secondary'>
                จำนวนทริป
              </Typography>
            </Stack>
          </Stack>
        </Grid>
      </Grid>
    </Box>
  );
};

VehicleDetail.propTypes = {
  data: PropTypes.object,
  month: PropTypes.instanceOf(Date),
};

const ReportModal = ({ open, data, month, onClose }) => {
  return (
    <Modal open={open} disableAutoFocus onClose={onClose}>
      <Fade in={open}>
        <Box p={2} sx={style}>
          <Card sx={{ width: '100%', maxWidth: '960px' }}>
            <Stack
              p={2}
              spacing={1}
              direction='row'
              justifyContent='start'
              alignItems='center'
              sx={{ borderBottom: '0.0625rem solid rgba(0, 0, 0, 0.085)' }}
            >
              <Typography variant='h4'>รายงานสรุปการขับขี่ประจำเดือน:</Typography>
              <Typography variant='h4' color='primary'>
                {format(month, 'MMMM')}
              </Typography>
            </Stack>

            <Stack
              p={2}
              spacing={1}
              direction='row'
              justifyContent='space-between'
              alignItems='center'
            >
              <DriverDetail data={data} />
              <VehicleDetail data={data} month={month} />
            </Stack>

            <Stack
              p={2}
              spacing={1}
              direction='row'
              justifyContent='end'
              alignItems='center'
              sx={{ borderTop: '0.0625rem solid rgba(0, 0, 0, 0.085)' }}
            >
              <Button variant='outlined' color='secondary' onClick={onClose}>
                ปิดรายงาน
              </Button>
            </Stack>
          </Card>
        </Box>
      </Fade>
    </Modal>
  );
};

ReportModal.defaultPropTypes = {
  month: new Date(),
};

ReportModal.propTypes = {
  open: PropTypes.bool.isRequired,
  data: PropTypes.object,
  month: PropTypes.instanceOf(Date),
  onClose: PropTypes.func.isRequired,
};

export default memo(ReportModal);
