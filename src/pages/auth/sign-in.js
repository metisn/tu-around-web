import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';

// @mui material components
import Grid from '@mui/material/Grid';

import Box from 'components/Box';
import Typography from 'components/Typography';
import Button from 'components/Button';
import Input from 'components/Input';

// @mui material components
import Switch from '@mui/material/Switch';

import authService from 'services/auth.service';

// Image
import bgImage from 'assets/images/bus-sunset.jpg';

const SignIn = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const [rememberMe, setRememberMe] = useState(false);

  // form validation rules
  const validationSchema = Yup.object().shape({
    email: Yup.string().email().required('Email is required'),
    password: Yup.string().required('Password is required'),
  });

  const formOptions = {
    defaultValues: {
      email: '',
      password: '',
    },
    resolver: yupResolver(validationSchema),
  };

  const { handleSubmit, control, formState } = useForm(formOptions);
  const { isSubmitting } = formState;

  const handleSetRememberMe = () => setRememberMe(!rememberMe);

  const { requestLogin } = authService;

  const handleLogin = ({ email, password }) => {
    return dispatch(requestLogin({ email, password }, () => navigate('/')));
  };

  let darkMode = false;

  return (
    <Grid
      container
      sx={{
        backgroundColor: ({ palette: { background, white } }) =>
          darkMode ? background.default : white.main,
      }}
    >
      <Grid item xs={12} lg={6}>
        <Box
          display={{ xs: 'none', lg: 'flex' }}
          width='calc(100% - 2rem)'
          height='calc(100vh - 2rem)'
          borderRadius='lg'
          ml={2}
          mt={2}
          sx={{
            backgroundImage: `url(${bgImage})`,
            backgroundPosition: 'center',
            backgroundSize: 'auto',
          }}
        />
      </Grid>
      <Grid item xs={11} sm={8} md={6} lg={4} xl={3} sx={{ mx: 'auto' }}>
        <Box display='flex' flexDirection='column' justifyContent='center' height='100vh'>
          <Box py={3} px={3} textAlign='center'>
            <>
              <Box mb={1} textAlign='center'>
                <Typography variant='h4' fontWeight='bold'>
                  Sign In
                </Typography>
              </Box>
              <Typography variant='body2' color='text'>
                Enter your email and password to sign in
              </Typography>
            </>
          </Box>
          <Box p={3}>
            <form onSubmit={handleSubmit(handleLogin)}>
              <Box mb={2}>
                <Controller
                  name='email'
                  control={control}
                  render={({ field: { onChange, value }, fieldState: { error } }) => (
                    <Input
                      type='text'
                      label='Email'
                      value={value}
                      onChange={onChange}
                      error={error ? true : false}
                      helperText={error?.message}
                      fullWidth
                    />
                  )}
                />
              </Box>
              <Box mb={2}>
                <Controller
                  name='password'
                  control={control}
                  render={({ field: { onChange, value }, fieldState: { error } }) => (
                    <Input
                      type='password'
                      label='Password'
                      value={value}
                      onChange={onChange}
                      error={error ? true : false}
                      helperText={error?.message}
                      fullWidth
                    />
                  )}
                />
              </Box>
              <Box display='flex' alignItems='center' ml={-1}>
                <Switch checked={rememberMe} onChange={handleSetRememberMe} />
                <Typography
                  variant='button'
                  fontWeight='regular'
                  color='text'
                  onClick={handleSetRememberMe}
                  sx={{ cursor: 'pointer', userSelect: 'none', ml: -1 }}
                >
                  &nbsp;&nbsp;Remember me
                </Typography>
              </Box>
              <Box mt={4} mb={1}>
                <Button
                  type='submit'
                  variant='gradient'
                  color='primary'
                  size='large'
                  disabled={isSubmitting}
                  fullWidth
                >
                  sign in
                </Button>
              </Box>
            </form>

            <Box textAlign='end'>
              <Typography
                variant='caption'
                onClick={() => navigate('/public/table')}
                sx={{ cursor: 'pointer' }}
              >
                ป้ายรถทั้งหมด
              </Typography>
            </Box>
          </Box>
        </Box>
      </Grid>
    </Grid>
  );
};

export default SignIn;
