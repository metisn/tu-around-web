/* eslint-disable no-unused-vars */
import Grid from '@mui/material/Grid';

// Material Dashboard 2 PRO React components
import Box from 'components/Box';
import Typography from 'components/Typography';

// Authentication layout components
import BasicLayout from 'layouts/components/BasicLayout';

// Images
import bgImage from 'assets/images/bg-sign-in-basic.jpeg';

function NotFound() {
  return (
    <BasicLayout image={bgImage}>
      <Typography variant='h1' color='white'>
        404 Not Found
      </Typography>
    </BasicLayout>
  );
}

export default NotFound;
