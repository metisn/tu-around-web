/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import Box from 'components/Box';

import Grid from '@mui/material/Grid';
import Card from '@mui/material/Card';

import MiniStatisticsCard from 'components/Card/miniStatic';
import { useDispatch } from 'react-redux';
import MapComponent from '../map';

import { Stack } from '@mui/material';

import List from '@mui/material/List';
import Typography from 'components/Typography';
import ListItem from '@mui/material/ListItem';
// import ListItemText from '@mui/material/ListItemText';
import Divider from '@mui/material/Divider';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import Icon from '@mui/material/Icon';

import { useStateLoading } from 'hooks';
import mapService from 'services/map.service';
import { useCallback, useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import PropTypes from 'prop-types';

import CircularProgress from '@mui/material/CircularProgress';

import map from 'app/reducers/map';
const { setVehiclesEmit } = map.actions;

// import { isEmpty } from 'draft-js/lib/DefaultDraftBlockRenderMap';

// eslint-disable-next-line no-unused-vars
const VehicleList = ({ data, index }) => {
  function getColor({ bus_line }) {
    switch (bus_line) {
      case 'yellow':
        return 'warning';
      case 'green':
        return 'success';
      case 'purple':
        return 'extra';
      case 'blue':
        return 'info';
      default:
        return 'primary';
    }
  }

  return (
    <>
      {index > 0 && <Divider />}
      <ListItem
        secondaryAction={
          <Icon color={data.status === 'online' ? 'success' : 'secondary'}>
            {data.status === 'online' ? 'wifi' : 'wifi_off'}
          </Icon>
        }
      >
        <ListItemAvatar>
          <Box
            variant='gradient'
            bgColor={getColor(data)}
            color={'white'}
            width='3rem'
            height='3rem'
            marginRight='auto'
            borderRadius='lg'
            display='flex'
            justifyContent='center'
            alignItems='center'
            shadow='md'
          >
            <Icon fontSize='small'>directions_bus</Icon>
          </Box>
        </ListItemAvatar>
        <Stack direction='row' alignItems='center' spacing={4}>
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
              height: '100%',
              justifyContent: 'space-evenly',
              alignItems: 'start',
            }}
          >
            <Stack spacing={1} direction='row'>
              <Typography variant='body2' sx={{ fontWeight: 500 }} color={getColor(data)}>
                {data.bus_name}
              </Typography>
            </Stack>
            <Stack spacing={1} direction='row'>
              <Typography variant='body2'>
                พิกัด: {`${data.obd_data?.lat || ''}, ${data.obd_data?.lon || ''}`}
              </Typography>
            </Stack>
          </Box>
        </Stack>
      </ListItem>
    </>
  );
};

const LoadingComponent = () => (
  <Box
    sx={{
      width: '100%',
      height: '100%',
      display: 'flex',
      justifyContent: 'center',
      alignItems: 'center',
      position: 'absolute',
      zIndex: 999,
    }}
  >
    <Box
      sx={{
        background: 'rgba(255,255,255,0.2)',
        backdropFilter: 'blur(16px)',
        width: '100%',
        height: '100%',
        position: 'absolute',
      }}
    />
    <CircularProgress size={52} />
  </Box>
);

const Home = () => {
  const dispatch = useDispatch();
  const [marker, setMarker] = useState(null);

  const { bus_lines, vehicles_emit } = useSelector((state) => state.map);

  // loader
  const [fetch, fetchMapApi] = useStateLoading(['loadingApi']);

  const { getMapLine, getMapStop, getMappicoVehicleLists, getAllVehicles } = mapService;

  const callRouteData = useCallback(async () => {
    fetchMapApi('loadingApi', () => dispatch(getMapLine()));
  }, [dispatch, fetchMapApi, getMapLine]);

  const callStopData = useCallback(async () => {
    let { data } = await dispatch(getMapStop());
    setMarker(data);
  }, [dispatch, getMapStop]);

  const callVehicleLists = useCallback(async () => {
    let { data } = await dispatch(getMappicoVehicleLists());
    let { results } = await dispatch(getAllVehicles());

    let allTracker = results.map((i) => {
      let filter = data.filter((obj) => obj.device_id == i.tracker_id);

      return { ...i, ...{ obd_data: {}, status: filter[0]?.status || 'offline' } };
    });

    dispatch(setVehiclesEmit(allTracker));
  }, [dispatch, getAllVehicles, getMappicoVehicleLists]);

  useEffect(() => {
    callRouteData();
    callStopData();
    callVehicleLists();
  }, [callRouteData, callStopData, callVehicleLists]);

  function getStatus(status, data) {
    if (status === 'total') return data.length;

    if (status === 'online') return data.filter((items) => items?.status === 'online').length;

    if (status === 'offline') return data.filter((items) => items?.status !== 'online').length;

    return data.filter((items) => items?.status === status).length;
  }

  function compare(a, b) {
    if (a.status === 'online') {
      return -1;
    }
    if (a.status === 'offline') {
      return 1;
    }
    return 0;
  }

  let sortVehicle = [...vehicles_emit].sort(compare);

  return (
    <Box py={2}>
      <Grid container spacing={2}>
        <Grid item xs={12} md={8} lg={9}>
          <Card sx={{ overflow: 'hidden' }}>
            {fetch.loadingApi && <LoadingComponent />}
            <MapComponent zoom={16} geoJson={bus_lines} marker={marker} vehicles={vehicles_emit} />
          </Card>
        </Grid>
        <Grid item xs={12} md={4} lg={3}>
          <Grid container direction='column' sx={{ width: '100%' }}>
            <Grid container direction='column' rowSpacing={2} sx={{ width: '100%' }}>
              <Grid item xs={12}>
                <MiniStatisticsCard
                  title={{ text: 'Total' }}
                  count={getStatus('total', vehicles_emit)}
                  icon={{ color: 'dark', component: 'subject' }}
                  direction='left'
                />
              </Grid>
              <Grid item xs={12}>
                <MiniStatisticsCard
                  title={{ text: 'Online' }}
                  count={getStatus('online', vehicles_emit)}
                  icon={{ color: 'success', component: 'wifi' }}
                  direction='left'
                />
              </Grid>
              <Grid item xs={12}>
                <MiniStatisticsCard
                  title={{ text: 'Offline' }}
                  count={getStatus('offline', vehicles_emit)}
                  icon={{ color: 'secondary', component: 'wifi_off' }}
                  direction='left'
                />
              </Grid>
            </Grid>

            <Grid item xs={12} sx={{ marginTop: '24px' }}>
              <Card sx={{ height: 'calc(100vh - 421px)' }}>
                <Stack
                  p={3}
                  direction='row'
                  justifyContent='start'
                  alignItems='center'
                  sx={{ borderBottom: '0.0625rem solid rgba(0, 0, 0, 0.085)', height: 42 }}
                >
                  <Typography variant='h6'>รายการรถในระบบ</Typography>
                </Stack>
                <Box p={2} sx={{ overflowY: 'scroll' }} className='no-scrollbars'>
                  <List dense>
                    {sortVehicle.length > 0 ? (
                      sortVehicle.map((item, index) => {
                        return <VehicleList key={index} data={item} index={index} />;
                      })
                    ) : (
                      <Typography variant='body2'>ไม่มีรถในระบบ</Typography>
                    )}
                  </List>
                </Box>
              </Card>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Box>
  );
};

export default Home;
