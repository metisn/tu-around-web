/* eslint-disable no-unused-vars */
'use strict';
import { useCallback, useEffect, useState } from 'react';

import Card from '@mui/material/Card';
import { Editor } from 'react-draft-wysiwyg';
import { ContentState, EditorState, convertToRaw } from 'draft-js';
import 'react-draft-wysiwyg/dist/react-draft-wysiwyg.css';
import { useDispatch } from 'react-redux';

import { Grid } from '@mui/material';
import Box from 'components/Box';
import { Stack } from '@mui/material';
import Typography from 'components/Typography';
import Switch from '@mui/material/Switch';
import SaveIcon from '@mui/icons-material/Save';
import Button from 'components/Button';
import { useStateLoading } from 'hooks';

import Skeleton from '@mui/material/Skeleton';

import signService from 'services/sign.service';

const { getNavView, postUpdateEmergencyView } = signService;
const SignAlert = () => {
  const dispatch = useDispatch();

  const [editorState, setEditorState] = useState(false);
  const [toggle, setToggle] = useState(false);

  const handleSetToggle = () => setToggle(!toggle);

  const onEditorStateChange = (editorState) => {
    setEditorState(editorState);
  };

  // loader
  const [fetching, fetchApi] = useStateLoading(['gettingNav', 'updatingEms']);

  const callUpdateEms = useCallback(
    async (body) => {
      fetchApi('updatingEms', () => dispatch(postUpdateEmergencyView(body)));
    },
    [dispatch, fetchApi]
  );

  const handleUpdateEmsView = useCallback(async () => {
    let text = editorState.getCurrentContent().getPlainText();

    callUpdateEms({ ems_text: text, toggle: toggle ? 1 : 0 });
  }, [callUpdateEms, editorState, toggle]);

  const callNavView = useCallback(() => {
    fetchApi('gettingNav', async () => {
      let { results } = await dispatch(getNavView());

      setToggle(findToggle(results[0]));

      const _contentState = ContentState.createFromText(results[0].emergency_text);
      const editorState = EditorState.createWithContent(_contentState);
      setEditorState(editorState);
    });
  }, [dispatch, fetchApi]);

  function findToggle({ toggle }) {
    switch (toggle) {
      case 0:
        return false;
      default:
        return true;
    }
  }

  useEffect(() => {
    callNavView();
  }, [callNavView]);

  return (
    <Box py={2}>
      <Card>
        {fetching.gettingNav ? (
          <Grid item xs={12} p={2}>
            <Skeleton
              variant='rectangular'
              width='100%'
              height='84px'
              style={{ borderRadius: 8 }}
            />
          </Grid>
        ) : (
          <Grid container spacing={2} flexDirection='column'>
            <Grid item xs={12}>
              <Stack
                p={3}
                direction='row'
                justifyContent='space-between'
                alignItems='center'
                sx={{ borderBottom: '0.0625rem solid rgba(0, 0, 0, 0.085)', height: 81 }}
              >
                <Typography variant='h5'>กำหนดข้อความฉุกเฉิน</Typography>
                <Stack direction='row' justifyContent='center' alignItems='center'>
                  <Typography variant='button'>
                    {toggle ? 'ปิดข้อความ' : 'เปิดข้อความฉุกเฉิน'}
                  </Typography>
                  <Switch checked={toggle} onChange={handleSetToggle} />
                </Stack>
              </Stack>
            </Grid>

            <Grid item xs={12} style={{ paddingTop: 0 }}>
              <Editor
                editorState={editorState}
                toolbarClassName='toolbarClassName'
                wrapperClassName='wrapperClassName'
                editorClassName='editorClassName'
                onEditorStateChange={onEditorStateChange}
                placeholder='กำหนดข้อความ...'
                toolbar={{
                  options: ['inline', 'blockType', 'fontSize', 'list', 'textAlign'],
                }}
              />

              <Stack
                p={2}
                spacing={1}
                direction='row'
                justifyContent='end'
                alignItems='center'
                sx={{ borderTop: '0.0625rem solid rgba(0, 0, 0, 0.085)' }}
              >
                <Button
                  variant='gradient'
                  color='primary'
                  startIcon={<SaveIcon />}
                  onClick={handleUpdateEmsView}
                  disabled={fetching.updatingEms}
                >
                  บันทึกข้อความ
                </Button>
              </Stack>
            </Grid>
          </Grid>
        )}
      </Card>
    </Box>
  );
};

export default SignAlert;
