/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import { useCallback, useEffect } from 'react';
import Card from '@mui/material/Card';
import Box from 'components/Box';
import Button from 'components/Button';
import { Grid } from '@mui/material';
import { Stack } from '@mui/material';
import { useDispatch } from 'react-redux';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';
import Input from 'components/Input';

import Typography from 'components/Typography';

import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';

import ListItemAvatar from '@mui/material/ListItemAvatar';
import Checkbox from '@mui/material/Checkbox';
import IconButton from '@mui/material/IconButton';
import { useDropzone } from 'react-dropzone';
import Icon from '@mui/material/Icon';

import Modal from '@mui/material/Modal';
import Fade from '@mui/material/Fade';

import format from 'date-fns/format';

import Divider from '@mui/material/Divider';
import { useState } from 'react';
import { isEmpty } from 'utils';
import { useStateLoading } from 'hooks';
import { useSelector } from 'react-redux';

import Skeleton from '@mui/material/Skeleton';

import signService from 'services/sign.service';

const { getSignMedias, postSignMedias, deleteSignMedias, getNavView, postUpdateNavView } =
  signService;

const style = {
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
};

const MediaList = ({ data, index, onCheck, onActionClick, isSelected }) => {
  function getTimeDisplay(duration) {
    if (isEmpty(duration)) {
      return 'N/A';
    }

    return `${duration} นาที`;
  }

  function getFileName(path) {
    let name = path.split('?')[0].split('/');
    return name[name.length - 1];
  }

  return (
    <>
      {index > 0 && <Divider />}
      <ListItem
        secondaryAction={
          <IconButton edge='end' aria-label='delete' onClick={() => onActionClick(data)}>
            <Icon>delete_forever</Icon>
          </IconButton>
        }
      >
        <Checkbox
          edge='start'
          tabIndex={-1}
          checked={isSelected(data || {})}
          onChange={() => onCheck(data)}
        />
        <ListItemAvatar>
          <div
            style={{
              width: 84,
              height: 84,
              backgroundColor: 'lightgray',
              margin: '0 12px',
              borderRadius: 8,
              display: 'flex',
              justifyContent: 'center',
              alignItems: 'center',
            }}
          >
            <Icon color='disabled' fontSize='large'>
              {data.mime_type === 'video/mp4' ? 'ondemand_video' : 'image'}
            </Icon>
          </div>
        </ListItemAvatar>
        <Stack direction='row' sx={{ height: 84 }} alignItems='center' spacing={4}>
          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
              height: '100%',
              justifyContent: 'space-evenly',
              alignItems: 'start',
            }}
          >
            <Stack spacing={1} direction='row'>
              <Typography variant='body2'>
                {data.mime_type === 'video/mp4' ? 'วีดีโอ' : 'รูปภาพ'}
              </Typography>
            </Stack>
            <Stack spacing={1} direction='row'>
              <Typography variant='body2'>{getTimeDisplay(data.duration)}</Typography>
            </Stack>
          </Box>

          <Box
            sx={{
              display: 'flex',
              flexDirection: 'column',
              height: '100%',
              justifyContent: 'space-evenly',
              alignItems: 'start',
            }}
          >
            <Stack spacing={1} direction='row'>
              <Typography variant='body2'>ชื่อไฟล์:</Typography>
              <Typography variant='body2'>{getFileName(data.file_path)}</Typography>
            </Stack>
            <Stack spacing={1} direction='row'>
              <Typography variant='body2'>วันที่และเวลา:</Typography>
              <Typography variant='body2'>
                {format(new Date(data.upload_date), 'dd/MM/yyyy hh:mm:ss')}
              </Typography>
            </Stack>
          </Box>
        </Stack>
      </ListItem>
    </>
  );
};

const SignDetail = () => {
  const dispatch = useDispatch();
  const { acceptedFiles, getRootProps, getInputProps, isDragActive } = useDropzone();
  const additionalClass = isDragActive ? 'active' : '';

  const [open, setModal] = useState(false);
  const [checked, setChecked] = useState([]);

  const { medias, nav_view } = useSelector((state) => state.sign);

  // loader
  const [fetching, fetchApi] = useStateLoading(['gettingMedia', 'gettingNav', 'updatingNav']);

  const callSignMedias = useCallback(async () => {
    fetchApi('gettingMedia', () => dispatch(getSignMedias()));
  }, [dispatch, fetchApi]);

  const callNavView = useCallback(async () => {
    fetchApi('gettingNav', () => dispatch(getNavView()));
  }, [dispatch, fetchApi]);

  const callUpdateSign = useCallback(
    async (body) => {
      fetchApi('updatingNav', () => dispatch(postUpdateNavView(body)));
    },
    [dispatch, fetchApi]
  );

  const validationSchema = Yup.object().shape({
    marquee_text: Yup.string().required('Running text required'),
    interval: Yup.number()
      .typeError('Only number accepted')
      .min(1, 'Min value 1')
      .max(30, 'Max value 30')
      .required('Interval required'),
  });

  const formOptions = {
    defaultValues: {
      marquee_text: 'Example text',
      interval: 1,
    },
    resolver: yupResolver(validationSchema),
  };

  const { handleSubmit, control, setValue } = useForm(formOptions);

  useEffect(() => {
    callSignMedias();
    callNavView();
  }, [callNavView, callSignMedias]);

  useEffect(() => {
    if (!isEmpty(nav_view)) {
      setValue('marquee_text', nav_view[0].marquee_text);
      setValue('interval', nav_view[0].ad_interval);

      const getSelected = nav_view[0].media_list
        .split(',')
        .map((i) => medias.find((obj) => obj.file_path === i));
      setChecked(getSelected);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [nav_view, setValue]);

  function handleItemSelected({ id, file_path }) {
    const selectedIndex = checked.findIndex((obj) => obj.id === id);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(checked, { id, file_path });
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(checked.slice(1));
    } else if (selectedIndex === checked.length - 1) {
      newSelected = newSelected.concat(checked.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        checked.slice(0, selectedIndex),
        checked.slice(selectedIndex + 1)
      );
    }

    setChecked(newSelected);
  }

  const isSelected = useCallback(
    ({ id }) => checked?.findIndex((obj) => obj?.id === id) !== -1,
    [checked]
  );

  const handleActionClick = useCallback(
    async (data) => {
      let response = await dispatch(deleteSignMedias({ id: data.id }));

      if (response) callSignMedias();
    },
    [callSignMedias, dispatch]
  );

  const handleUploadFiles = useCallback(
    async (files) => {
      setModal(false);

      let formData = new FormData();
      formData.append('file', files[0]);

      let response = await dispatch(postSignMedias(formData));
      if (response) callSignMedias();
    },
    [callSignMedias, dispatch]
  );

  useEffect(() => {
    if (!isEmpty(acceptedFiles)) handleUploadFiles(acceptedFiles);
  }, [acceptedFiles, handleUploadFiles]);

  const handleUpdateNavView = (body) => {
    let arr = checked?.map((obj) => {
      let name = obj.file_path.split('?')[0].split('/');
      return (
        '/' + name[name.length - 3] + '/' + name[name.length - 2] + '/' + name[name.length - 1]
      );
    });
    let newStr = arr.map((i) => i).join(',');

    callUpdateSign({ ...body, media_list: newStr });
  };

  return (
    <Box py={2}>
      <Grid container spacing={2}>
        <Grid item xs={12} md={8}>
          <Card>
            <Stack
              p={3}
              direction='row'
              justifyContent='space-between'
              alignItems='center'
              sx={{ borderBottom: '0.0625rem solid rgba(0, 0, 0, 0.085)' }}
            >
              <Typography variant='h5'>จัดการรูปภาพและวีดีโอ</Typography>
              <Button
                variant='gradient'
                color='info'
                size='small'
                startIcon={<Icon>cloud_upload</Icon>}
                onClick={() => setModal(true)}
              >
                อับโหลด
              </Button>
            </Stack>

            <Box p={3}>
              <List dense>
                {fetching.gettingMedia ? (
                  <Stack direction='row'>
                    <Skeleton
                      variant='rectangular'
                      width='15%'
                      height='84px'
                      style={{ borderRadius: 8 }}
                    />
                    <Box
                      sx={{
                        width: '100%',
                      }}
                      pl={3}
                    >
                      <Skeleton width='100%' height='52px' />
                      <Skeleton width='100%' height='32px' />
                    </Box>
                  </Stack>
                ) : medias.length > 0 ? (
                  medias?.map((item, index) => {
                    return (
                      <MediaList
                        key={index}
                        data={item}
                        index={index}
                        onCheck={handleItemSelected}
                        onActionClick={handleActionClick}
                        isSelected={isSelected}
                      />
                    );
                  })
                ) : (
                  <Typography sx={{ textAlign: 'center' }}>No media recorded</Typography>
                )}
              </List>
            </Box>
          </Card>
        </Grid>
        <Grid item xs={12} md={4}>
          <Card sx={{ position: 'sticky', top: '94px' }}>
            <Stack
              p={3}
              direction='row'
              justifyContent='start'
              alignItems='center'
              sx={{ borderBottom: '0.0625rem solid rgba(0, 0, 0, 0.085)', height: 81 }}
            >
              <Typography variant='h5'>ตั้งค่าหน้าจอแสดงผล</Typography>
            </Stack>
            <Box p={3}>
              <form onSubmit={handleSubmit(handleUpdateNavView)}>
                {fetching.gettingNav ? (
                  <Stack direction='column'>
                    <Skeleton width='100%' height='52px' />
                    <Skeleton width='100%' height='52px' />
                  </Stack>
                ) : (
                  <Stack spacing={3}>
                    <Controller
                      name='marquee_text'
                      control={control}
                      render={({ field: { onChange, value }, fieldState: { error } }) => (
                        <Input
                          type='text'
                          label='ข้อความโฆษณาวิ่ง (ไม่เกิน 100 ตัวอักษร)'
                          value={value}
                          multiline
                          onChange={onChange}
                          error={error ? true : false}
                          helperText={error?.message}
                          fullWidth
                        />
                      )}
                    />
                    <Controller
                      name='interval'
                      control={control}
                      render={({ field: { onChange, value }, fieldState: { error } }) => (
                        <Input
                          type='number'
                          label='ระยะเวลาแสดงผลรูปภาพ (วินาที)'
                          value={value}
                          onChange={onChange}
                          error={error ? true : false}
                          helperText={error?.message}
                          fullWidth
                        />
                      )}
                    />
                    <Typography variant='button'>
                      จำนวนไฟล์ที่เลือกแสดง: {checked.length}
                    </Typography>
                  </Stack>
                )}
                <Stack pt={3} spacing={1} direction='row' justifyContent='end'>
                  <Button variant='text' color='secondary' onClick={() => setChecked([])}>
                    ล้างข้อมูล
                  </Button>
                  <Button
                    type='submit'
                    variant='contained'
                    color='primary'
                    startIcon={<Icon>save</Icon>}
                    disabled={fetching.updatingNav}
                  >
                    บันทึกข้อมูลทั้งหมด
                  </Button>
                </Stack>
              </form>
            </Box>
          </Card>
        </Grid>
      </Grid>

      <Modal open={open} disableAutoFocus onClose={() => setModal(false)}>
        <Fade in={open}>
          <Box sx={style}>
            <Card>
              <Stack
                p={3}
                direction='row'
                justifyContent='space-between'
                alignItems='center'
                sx={{ borderBottom: '0.0625rem solid rgba(0, 0, 0, 0.085)' }}
              >
                <Typography variant='h6'>อัพโหลดไฟล์</Typography>
                <IconButton aria-label='close_modal' size='small' onClick={() => setModal(false)}>
                  <Icon>clear</Icon>
                </IconButton>
              </Stack>

              <Box p={2}>
                <div {...getRootProps({ className: `dropzone ${additionalClass}` })}>
                  <input {...getInputProps()} />
                  <p>เลือกไฟล์ หรือ ลากไฟล์ที่ต้องการอัปโหลด</p>
                </div>
              </Box>
            </Card>
          </Box>
        </Fade>
      </Modal>
    </Box>
  );
};

export default SignDetail;
