/* eslint-disable react/prop-types */
/* eslint-disable no-unused-vars */
import { memo, useEffect, useState } from 'react';
import PropTypes from 'prop-types';

import { Grid, Stack } from '@mui/material';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Modal from '@mui/material/Modal';
import Fade from '@mui/material/Fade';
import Card from '@mui/material/Card';
import Autocomplete from '@mui/material/Autocomplete';
import FormField from 'components/FormField';

import Button from 'components/Button';
import Input from 'components/Input';

import Icon from '@mui/material/Icon';

// import Typography from 'components/Typography';
import { useForm, Controller } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import * as Yup from 'yup';

import Box from 'components/Box';
import Typography from 'components/Typography';
import { isEmpty } from 'utils';

let host = process.env.REACT_APP_API_URL || '';

const style = {
  width: '100%',
  position: 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  display: 'flex',
  justifyContent: 'center',
};

const status = [
  { id: 'red', label: 'สาย 1A สีแดง' },
  { id: 'yellow', label: 'สาย 1B สีเหลือง' },
  { id: 'green', label: 'สาย 2 สีเขียว' },
  { id: 'purple', label: 'สาย 3 สีม่วง' },
  { id: 'blue', label: 'สาย 5 สีฟ้า' },
];

// eslint-disable-next-line no-unused-vars
const VehicleDetail = memo(({ value, data, index, control, openFile }) => {
  return (
    <Box
      role='tabpanel'
      hidden={value !== index}
      id={`view-tabpanel-${index}`}
      aria-labelledby={`view-tab-${index}`}
    >
      <Grid p={3} container flexDirection='row'>
        <Grid
          item
          xs={12}
          md={3}
          sx={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'start',
            alignItems: 'start',
          }}
          mb={3}
        >
          {isEmpty(data.bus_img) ? (
            <div
              style={{
                width: 180,
                height: 180,
                backgroundColor: 'lightgray',
                borderRadius: 8,

                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Icon color='disabled' fontSize='large'>
                image
              </Icon>
            </div>
          ) : (
            <img
              src={data.bus_img}
              alt='bus_img'
              style={{
                width: 180,
                height: 180,
                backgroundColor: 'lightgray',
                borderRadius: 8,

                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                boxShadow: '0rem 1.25rem 1.6875rem 0rem rgb(0 0 0 / 5%)',
              }}
            />
          )}
          <Button
            onClick={() => openFile('vehicle')}
            iconOnly
            style={{ position: 'absolute', top: '55.6%', left: '18.5%' }}
          >
            <Icon>edit</Icon>
          </Button>
        </Grid>

        <Grid item xs={12} md={9} sx={{ maxHeight: '200px' }}>
          <Grid item xs={12} mb={2}>
            <Stack spacing={1} direction='row' justifyContent='center' alignItems='center'>
              <Controller
                name='plate_alphabet'
                control={control}
                render={({ field: { onChange, value }, fieldState: { error } }) => (
                  <Input
                    type='text'
                    label='ทะเบียนรถ (หมวดตัวอักษร)'
                    value={value}
                    onChange={onChange}
                    error={error ? true : false}
                    helperText={error?.message}
                    fullWidth
                  />
                )}
              />

              <Typography variant='caption'>-</Typography>

              <Controller
                name='plate_number'
                control={control}
                render={({ field: { onChange, value }, fieldState: { error } }) => (
                  <Input
                    type='text'
                    label='ทะเบียนรถ (หมวดหมายเลข)'
                    value={value}
                    onChange={onChange}
                    error={error ? true : false}
                    helperText={error?.message}
                    fullWidth
                  />
                )}
              />
            </Stack>
          </Grid>
          <Grid container spacing={2} my={1}>
            <Grid item xs={12} md={6}>
              <Controller
                name='tracker_id'
                control={control}
                render={({ field: { onChange, value }, fieldState: { error } }) => (
                  <Input
                    type='text'
                    label='Tracker ID'
                    value={value}
                    onChange={onChange}
                    error={error ? true : false}
                    helperText={error?.message}
                    fullWidth
                    disabled
                  />
                )}
              />
            </Grid>
            <Grid item xs={12} md={6}>
              <Controller
                name='bus_name'
                control={control}
                render={({ field: { onChange, value }, fieldState: { error } }) => (
                  <Input
                    type='text'
                    label='ชื่อรถ'
                    value={value}
                    onChange={onChange}
                    error={error ? true : false}
                    helperText={error?.message}
                    fullWidth
                  />
                )}
              />
            </Grid>
          </Grid>
          <Grid container spacing={2} mt={2}>
            <Grid item xs={12} md={6}>
              <Controller
                name='unit_id'
                control={control}
                render={({ field: { onChange, value }, fieldState: { error } }) => (
                  <Input
                    type='text'
                    label='หมายเลขตัวถัง'
                    value={value}
                    onChange={onChange}
                    error={error ? true : false}
                    helperText={error?.message}
                    fullWidth
                  />
                )}
              />
            </Grid>
            <Grid item xs={12} md={6}>
              <Controller
                name='bus_line'
                control={control}
                render={({ field: { onChange, value }, fieldState: { error } }) => (
                  <Autocomplete
                    options={status}
                    value={status.find((obj) => obj.id == value)}
                    isOptionEqualToValue={(option, value) => option.id === value.id}
                    getOptionLabel={(option) => option.label}
                    onChange={(event, value) => onChange(value?.id)}
                    renderInput={(params) => <FormField {...params} label='สายรถ' />}
                  />
                )}
              />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Box>
  );
});

VehicleDetail.displayName = 'VehicleDetail';

VehicleDetail.propTypes = {
  index: PropTypes.number.isRequired,
  data: PropTypes.object,
  value: PropTypes.number.isRequired,
  control: PropTypes.object,
};

const DriverDetail = memo(({ value, data, index, control, openFile }) => {
  return (
    <Box
      role='tabpanel'
      hidden={value !== index}
      id={`view-tabpanel-${index}`}
      aria-labelledby={`view-tab-${index}`}
    >
      <Grid p={3} container flexDirection='row'>
        <Grid
          item
          xs={12}
          md={3}
          sx={{
            display: 'flex',
            flexDirection: 'column',
            justifyContent: 'start',
            alignItems: 'start',
          }}
          mb={3}
        >
          {isEmpty(data.driver_img) ? (
            <div
              style={{
                width: 180,
                height: 180,
                backgroundColor: 'lightgray',
                borderRadius: 8,

                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
              }}
            >
              <Icon color='disabled' fontSize='large'>
                image
              </Icon>
            </div>
          ) : (
            <img
              src={data.driver_img}
              alt='driver_img'
              style={{
                width: 180,
                height: 180,
                backgroundColor: 'lightgray',
                borderRadius: 8,

                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
                boxShadow: '0rem 1.25rem 1.6875rem 0rem rgb(0 0 0 / 5%)',
              }}
            />
          )}
          <Button
            onClick={() => openFile('driver')}
            iconOnly
            style={{ position: 'absolute', top: '55.6%', left: '18.5%' }}
          >
            <Icon>edit</Icon>
          </Button>
        </Grid>

        <Grid item xs={12} md={9} sx={{ maxHeight: '200px' }}>
          <Grid item xs={12} mb={2}>
            <Stack spacing={1} direction='row' justifyContent='center' alignItems='center'>
              <Controller
                name='driver_fname'
                control={control}
                render={({ field: { onChange, value }, fieldState: { error } }) => (
                  <Input
                    type='text'
                    label='ชื่อ'
                    value={value}
                    onChange={onChange}
                    error={error ? true : false}
                    helperText={error?.message}
                    fullWidth
                  />
                )}
              />
              <Typography variant='caption'>-</Typography>
              <Controller
                name='driver_lname'
                control={control}
                render={({ field: { onChange, value }, fieldState: { error } }) => (
                  <Input
                    type='text'
                    label='นามสกุล'
                    value={value}
                    onChange={onChange}
                    error={error ? true : false}
                    helperText={error?.message}
                    fullWidth
                  />
                )}
              />
            </Stack>
          </Grid>
          <Grid container spacing={2} my={1}>
            <Grid item xs={12}>
              <Controller
                name='driver_phone'
                control={control}
                render={({ field: { onChange, value }, fieldState: { error } }) => (
                  <Input
                    type='text'
                    label='เบอร์โทรศัพท์'
                    value={value}
                    onChange={onChange}
                    error={error ? true : false}
                    helperText={error?.message}
                    fullWidth
                  />
                )}
              />
            </Grid>
          </Grid>
          <Grid container spacing={2} mt={2}>
            <Grid item xs={12}>
              <Controller
                name='driver_license'
                control={control}
                render={({ field: { onChange, value }, fieldState: { error } }) => (
                  <Input
                    type='text'
                    label='เลขใบขับขี่'
                    value={value}
                    onChange={onChange}
                    error={error ? true : false}
                    helperText={error?.message}
                    fullWidth
                  />
                )}
              />
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </Box>
  );
});

DriverDetail.displayName = 'DriverDetail';

DriverDetail.propTypes = {
  index: PropTypes.number.isRequired,
  data: PropTypes.object,
  value: PropTypes.number.isRequired,
  control: PropTypes.object,
};

const ManageVehicleModal = ({
  openModal,
  data,
  onClose,
  onSubmit,
  loading,
  fileUpload,
  setUaction,
}) => {
  // console.log('file: modal.js ~ line 192 ~ data', data);
  // form validation rules
  const validationSchema = Yup.object().shape({
    plate_alphabet: Yup.string().required('Plate alphabet is required'),
  });

  const formOptions = {
    defaultValues: {
      plate_alphabet: '',
    },
    resolver: yupResolver(validationSchema),
  };

  const { handleSubmit, control, formState, setValue } = useForm(formOptions);
  const { isSubmitting } = formState;

  const [tabValue, setTabValue] = useState(0);

  function handleUpload(uaction) {
    setUaction(uaction);
    fileUpload();
  }

  useEffect(() => {
    if (!isEmpty(data)) {
      //  set form value base on data selected
      setValue('imsi', data.imsi);
      setValue('plate_alphabet', data.plate_alphabet);
      setValue('plate_number', data.plate_number);
      setValue('tracker_id', data.tracker_id);
      setValue('bus_name', data.bus_name);
      setValue('unit_id', data.unit_id);
      setValue('bus_line', data.bus_line);
      setValue('driver_fname', data.driver_fname);
      setValue('driver_lname', data.driver_lname);
      setValue('driver_phone', data.driver_phone);
      setValue('driver_license', data.driver_license);
    }
  }, [data, setValue]);

  const handleChange = (event, newValue) => {
    setTabValue(newValue);
  };

  function a11yProps(index) {
    return {
      id: `view-tab-${index}`,
      'aria-controls': `view-tabpanel-${index}`,
    };
  }

  return (
    <Modal open={openModal} disableAutoFocus onClose={onClose}>
      <Fade in={openModal}>
        <Box p={2} sx={style}>
          <Card sx={{ width: '100%', maxWidth: '860px' }}>
            <form onSubmit={handleSubmit(onSubmit)}>
              <Tabs
                value={tabValue}
                onChange={handleChange}
                aria-label='basic tabs example'
                sx={{ borderBottom: '0.0625rem solid rgba(0, 0, 0, 0.085)' }}
              >
                <Tab label='ข้อมูลรถ' {...a11yProps(0)} sx={{ fontSize: '1rem' }} />
                <Tab label='ข้อมูลคนขับ' {...a11yProps(1)} />
              </Tabs>
              <VehicleDetail
                value={tabValue}
                data={data}
                index={0}
                control={control}
                openFile={handleUpload}
              />
              <DriverDetail
                value={tabValue}
                data={data}
                index={1}
                control={control}
                openFile={handleUpload}
              />

              <Stack
                p={2}
                spacing={1}
                direction='row'
                justifyContent='end'
                alignItems='center'
                sx={{ borderTop: '0.0625rem solid rgba(0, 0, 0, 0.085)' }}
              >
                <Button variant='text' color='secondary' onClick={onClose}>
                  ปิด
                </Button>
                <Button type='submit' variant='gradient' color='primary' disabled={loading}>
                  บันทึกข้อมูล
                </Button>
              </Stack>
            </form>
          </Card>
        </Box>
      </Fade>
    </Modal>
  );
};

ManageVehicleModal.propTypes = {
  openModal: PropTypes.bool.isRequired,
  data: PropTypes.object,
  onClose: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired,
  loading: PropTypes.bool,
};

export default memo(ManageVehicleModal);
