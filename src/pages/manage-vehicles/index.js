/* eslint-disable no-unused-vars */
import { useState, useEffect, useCallback } from 'react';
import DataTable from 'components/Table';
import Card from '@mui/material/Card';
import Box from 'components/Box';
import Button from 'components/Button';
import Input from 'components/Input';

import { Grid } from '@mui/material';
import Autocomplete from '@mui/material/Autocomplete';
import FormField from 'components/FormField';
import SearchIcon from '@mui/icons-material/Search';
import TableCell from '@mui/material/TableCell';
import Stack from '@mui/material/Stack';
import { format } from 'date-fns';
import { Icon } from '@mui/material';
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import useDeepCompareEffect from 'use-deep-compare-effect';
import ManageVehicleModal from './modal';

import { isEmpty } from 'utils';

// service caller
import mapService from 'services/map.service';
import manageService from 'services/manage.service';
// reducer action
import manage from 'app/reducers/manage';
import { useStateLoading } from 'hooks';

import { useDropzone } from 'react-dropzone';

const status = [
  { id: 'online', label: 'Online' },
  { id: 'offline', label: 'Offline' },
];

// table info
const headCells = [
  { id: 'no', label: 'ลำดับ', align: 'left' },
  { id: 'plate_id', label: 'ทะเบียนรถ', align: 'left' },
  { id: 'bus_name', label: 'ชื่อรถ', align: 'left' },
  { id: 'tracker_id', label: 'Tracker ID', align: 'left' },
  { id: 'status', label: 'สถานะ', align: 'left' },
  { id: 'driver_name', label: 'ชื่อผู้ขับ', align: 'left' },
  { id: 'bus_line', label: 'สายรถ', align: 'left' },
  { id: 'latest_online', label: 'เวลาออนไลน์ล่าสุด', align: 'left' },
  { id: 'href', label: '', align: 'right' },
];

const bodyItems = ({ data, index, callAction }) => {
  function getColor({ bus_line }) {
    switch (bus_line) {
      case 'yellow':
        return 'warning';
      case 'green':
        return 'success';
      case 'purple':
        return 'extra';
      case 'blue':
        return 'info';
      default:
        return 'primary';
    }
  }

  return (
    <>
      <TableCell component='th' scope='row'>
        {index}
      </TableCell>
      <TableCell component='th' scope='row'>
        {data.plate_alphabet + '-' + data.plate_number}
      </TableCell>
      <TableCell>{data.bus_name}</TableCell>
      <TableCell>{data.tracker_id}</TableCell>
      <TableCell>
        <Stack direction='row' sx={{ width: '100%' }} spacing={1}>
          <p>{data.status.charAt(0).toUpperCase() + data.status.slice(1)}</p>
          <Icon fontSize='medium' color={data.status === 'online' ? 'success' : 'secondary'}>
            {data.status === 'online' ? 'wifi' : 'wifi_off'}
          </Icon>
        </Stack>
      </TableCell>
      <TableCell>{data.driver_fname + ' ' + data.driver_lname}</TableCell>
      <TableCell style={{ color: data.bus_line, fontWeight: 600 }}>
        <Box
          variant='gradient'
          bgColor={getColor(data)}
          color={'white'}
          width='2.5rem'
          height='2.5rem'
          marginRight='auto'
          borderRadius='lg'
          display='flex'
          justifyContent='center'
          alignItems='center'
          shadow='md'
        >
          <Icon fontSize='small'>directions_bus</Icon>
        </Box>
      </TableCell>
      <TableCell>{format(new Date(data.latest_online), 'dd/MM/yyyy, HH:mm:ss') || 'N/A'}</TableCell>
      <TableCell align='right'>
        <Button
          variant='contained'
          color='success'
          startIcon={<Icon>descriptionn</Icon>}
          onClick={() => callAction(data)}
        >
          ดูข้อมูล
        </Button>
      </TableCell>
    </>
  );
};

const ManageVehicle = () => {
  const dispatch = useDispatch();

  const [openModal, setModal] = useState(false);
  const [selectedData, setSelectedData] = useState(null);

  const [search, setSearch] = useState('');
  const [searchStatus, setSearchStatus] = useState({});
  const [uaction, setUaction] = useState('');

  const [filterResult, setFilterResult] = useState([]);

  const { getAllVehicles, postUpdateVehicle, postUploadImage } = manageService;
  const { getMappicoVehicleLists } = mapService;
  const { setVehicleLists } = manage.actions;
  const { vehicle_lists } = useSelector((state) => state.manage);

  const { acceptedFiles, open } = useDropzone();

  useEffect(() => {
    if (!isEmpty(acceptedFiles[0])) callUploadImage(acceptedFiles);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [acceptedFiles]);

  // loader
  const [fetching, fetchApi] = useStateLoading(['gettingVehicle', 'updatingVehicle']);

  const searchData = () => {
    let filteredRows = vehicle_lists.filter((row) => {
      return (
        row.bus_name.toLowerCase().includes(search.toLowerCase()) ||
        row.driver_fname.toLowerCase().includes(search.toLowerCase()) ||
        row.driver_lname.toLowerCase().includes(search.toLowerCase()) ||
        row.plate_alphabet.toLowerCase().includes(search.toLowerCase()) ||
        row.plate_number.toString().toLowerCase().includes(search.toLowerCase()) ||
        row.tracker_id.toLowerCase().includes(search.toLowerCase())
      );
    });

    if (!isEmpty(searchStatus)) {
      filteredRows = filteredRows.filter((row) => row.status === searchStatus);
    }

    setFilterResult(filteredRows);
  };

  const callVehicleLists = useCallback(() => {
    fetchApi('gettingVehicle', async () => {
      let { results } = await dispatch(getAllVehicles());

      if (!isEmpty(results)) {
        let { data } = await dispatch(getMappicoVehicleLists());

        let filterVehicle = results.map((i) => {
          let filter = data.filter((obj) => obj.device_id == i.tracker_id);

          return {
            ...i,
            ...{
              status: filter[0]?.status || 'offline',
              latest_online: filter[0]?.latest_online || null,
              latest_address: filter[0]?.latest_address || null,
            },
          };
        });

        dispatch(setVehicleLists(filterVehicle));

        return filterVehicle;
      }
    });
  }, [dispatch, fetchApi, getAllVehicles, getMappicoVehicleLists, setVehicleLists]);

  const callUploadImage = useCallback(
    async (files) => {
      let formData = new FormData();
      formData.append('file', files[0]);
      formData.append('imsi', selectedData?.imsi);
      formData.append('uaction', uaction);

      let res = await dispatch(postUploadImage(formData));

      if (res) {
        let newImage = { ...selectedData };

        if (uaction === 'vehicle') {
          Object.assign(newImage, { bus_img: `/assets/img/${res.name}` });
        }

        if (uaction === 'driver') {
          Object.assign(newImage, { driver_img: `/assets/img/${res.name}` });
        }

        setSelectedData(newImage);
        callVehicleLists();
      }
    },
    [callVehicleLists, dispatch, postUploadImage, selectedData, uaction]
  );

  useEffect(() => {
    callVehicleLists();
  }, [callVehicleLists]);

  function toggleModalData(data) {
    setSelectedData(data);
    setModal(true);
  }

  const callUpdateVehicle = useCallback(
    async (body) => {
      fetchApi('updatingVehicle', async () => {
        let status = await dispatch(postUpdateVehicle(body));

        if (status) {
          setModal(false);
          callVehicleLists();
        }
      });
    },
    [callVehicleLists, dispatch, fetchApi, postUpdateVehicle]
  );

  function clearSearch() {
    setSearch('');
    setSearchStatus(null);
    setFilterResult([]);
  }

  useDeepCompareEffect(() => {
    if (!isEmpty(search) || !isEmpty(searchStatus)) searchData();
  }, [vehicle_lists]);

  return (
    <Box py={2}>
      <Grid container spacing={2} flexDirection='column'>
        <Grid item xs={12}>
          <Card>
            <Stack p={3} spacing={2} direction='row' sx={{ maxHeight: 92 }}>
              <Input
                type='text'
                label='คำค้นหา'
                value={search}
                onChange={(event) => setSearch(event.target.value)}
              />
              <Autocomplete
                options={status}
                value={status.find((obj) => obj.id == searchStatus) || null}
                isOptionEqualToValue={(option, value) => option.id === value.id}
                getOptionLabel={(option) => option.label}
                onChange={(event, value) => setSearchStatus(value?.id)}
                renderInput={(params) => <FormField {...params} label='สถานะ' />}
              />
              <Button
                variant='gradient'
                color='primary'
                startIcon={<SearchIcon />}
                onClick={searchData}
              >
                ค้นหา
              </Button>
              <Button onClick={clearSearch}>ล้างข้อมูล</Button>
            </Stack>
          </Card>
        </Grid>
        <Grid item xs={12}>
          <Card sx={{ overflow: 'hidden' }}>
            <DataTable
              TableBodyItem={bodyItems}
              headCellsData={headCells}
              bodyCellsData={vehicle_lists}
              filterData={filterResult}
              handleAction={toggleModalData}
              isLoading={fetching.gettingVehicle}
            />
          </Card>
        </Grid>
      </Grid>

      <ManageVehicleModal
        openModal={openModal}
        data={selectedData}
        onClose={() => setModal(false)}
        onSubmit={callUpdateVehicle}
        loading={fetching.updatingVehicle}
        fileUpload={open}
        setUaction={setUaction}
      />
    </Box>
  );
};

export default ManageVehicle;
