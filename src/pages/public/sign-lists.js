import { useState, useEffect, useCallback } from 'react';
import Button from 'components/Button';
import DataTable from 'components/Table';
import TableCell from '@mui/material/TableCell';
// @mui material components
import Icon from '@mui/material/Icon';

import Box from 'components/Box';
import Typography from 'components/Typography';
import { useNavigate } from 'react-router-dom';

import Card from '@mui/material/Card';
import { Grid, Stack, Switch } from '@mui/material';

import { useDispatch } from 'react-redux';
import { useStateLoading } from 'hooks';
import signService from 'services/sign.service';

const SignLists = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [bodyData, setBodyData] = useState([]);
  const [debugState, setDebugState] = useState(false);

  // loader
  const [fetching, fetchApi] = useStateLoading(['gettingStations']);

  const { getStationList } = signService;

  const callStation = useCallback(
    async () =>
      fetchApi('gettingStations', async () => {
        let { results } = await dispatch(getStationList());

        setBodyData(results);
      }),
    [dispatch, fetchApi, getStationList]
  );

  useEffect(() => {
    callStation();
  }, [callStation]);

  // table info
  const headCells = [
    { id: 'no', label: 'No', align: 'left' },
    // { id: 'id', label: 'ID', align: 'left' },
    { id: 'station_name', label: 'ชื่อป้าย', align: 'left' },
    { id: 'location', label: 'พิกัด', align: 'left' },
    { id: 'href', label: '', align: 'right' },
  ];

  const bodyItems = ({ data, index }) => {
    return (
      <>
        <TableCell component='th' scope='row'>
          {index}
        </TableCell>
        {/* <TableCell component='th' scope='row'>
          {data.id}
        </TableCell> */}
        <TableCell component='th' scope='row'>
          {data.Station_Name}
        </TableCell>
        <TableCell>{data.Lat + ', ' + data.Lon}</TableCell>
        <TableCell align='right'>
          <Button
            variant='contained'
            color='warning'
            iconOnly
            onClick={() =>
              navigate(
                `../table/view/${data.Station_Name}&id=${data.id}&lat=${data.Lat}&lon=${data.Lon}`
              )
            }
          >
            <Icon>present_to_all</Icon>
          </Button>
          {'  '}
          {debugState && (
            <Button
              variant='contained'
              color='secondary'
              iconOnly
              onClick={() =>
                navigate(
                  `../table/view/debug/${data.Station_Name}&id=${data.id}&lat=${data.Lat}&lon=${data.Lon}`
                )
              }
            >
              <Icon>adb</Icon>
            </Button>
          )}
        </TableCell>
      </>
    );
  };

  const handleSetToggle = () => setDebugState(!debugState);

  return (
    <Box p={3}>
      <Grid container direction={'column'}>
        <Grid item xs={12}>
          <Typography variant='h4' fontWeight='bold'>
            รายการป้ายทั้งหมดในระบบ
          </Typography>
        </Grid>

        <Grid mt={3} item xs={12}>
          <Card sx={{ overflow: 'hidden' }}>
            <DataTable
              TableBodyItem={bodyItems}
              headCellsData={headCells}
              bodyCellsData={bodyData}
              isLoading={fetching.gettingStations}
            />
          </Card>
        </Grid>

        <Stack direction='row' justifyContent='end' alignItems='center'>
          <Typography variant='button'>โหมดทดสอบ</Typography>
          <Switch checked={debugState} onChange={handleSetToggle} />
          <Typography variant='button'>{debugState ? 'เปิด' : 'ปิด'}</Typography>
        </Stack>
      </Grid>
    </Box>
  );
};

export default SignLists;
