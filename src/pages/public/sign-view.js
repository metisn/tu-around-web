/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */

import { memo, useEffect, useCallback, useMemo } from 'react';
import { useParams } from 'react-router-dom';

import Box from 'components/Box';
import Grid from '@mui/material/Grid';
import Typography from 'components/Typography';

import Skeleton from '@mui/material/Skeleton';

import Stack from '@mui/material/Stack';
import { useState } from 'react';

import tuLogo from 'assets/images/tu-logo.webp';
import Marquee from 'react-fast-marquee';
import MapComponent from 'pages/map';

import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';

import { Icon } from '@mui/material';

import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';

import mapService from 'services/map.service';
import map from 'app/reducers/map';
import sign from 'app/reducers/sign';

import signService from 'services/sign.service';

// import static map geojson
import staticMap from 'utils/constants/map.json';

import { Carousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css'; // requires a loader

import { useSocketPublic } from 'hooks';
import { isEmpty } from 'utils';

import pmVeryGood from 'assets/pm/pm-verygood.png';
import pmGood from 'assets/pm/pm-good.png';
import pmMedium from 'assets/pm/pm-medium.png';
import pmBad from 'assets/pm/pm-bad.png';
import pmVeryBad from 'assets/pm/pm-verybad.png';

import './overrride-ads.css';

const { setPublicVehiclesEmit } = map.actions;
const { setReload } = sign.actions;

let host = process.env.REACT_APP_API_URL || '';

const SignView = memo(() => {
  const { stationName, stationID, lat, lon } = useParams();

  const socket = useSocketPublic(stationID);
  const dispatch = useDispatch();

  const [dateState, setDateState] = useState(new Date());
  const [viewData, setViewData] = useState(null);
  const [mediaLists, setMediaLists] = useState([]);
  const [displayMarquee, setDisplayMarquee] = useState(false);

  const [adsDuration, setAdsDuration] = useState(0);

  const [busIn, setBusIn] = useState([]);

  const { public_vehicles_emit, public_vehicles_in } = useSelector((state) => state.map);
  const { nav_view, reload } = useSelector((state) => state.sign);

  const { getMappicoVehicleLists, getAllVehicles } = mapService;
  const { getNavView } = signService;

  const bounds = [
    [14.063508835063175, 100.593458951714808],
    [14.079139070622741, 100.593458951714808],
    [14.079139070622741, 100.620128724278004],
    [14.063508835063175, 100.620128724278004],
  ];

  useEffect(() => {
    return () => {
      socket?.close();
    };
  }, [socket]);

  const getMergeVehicleLists = useCallback(async () => {
    let { data } = await dispatch(getMappicoVehicleLists());
    let { results } = await dispatch(getAllVehicles());

    let allTracker = results.map((i) => {
      let filter = data.filter((obj) => obj.device_id == i.tracker_id);

      return { ...i, ...{ obd_data: {}, status: filter[0]?.status || 'offline' } };
    });

    dispatch(setPublicVehiclesEmit(allTracker));
  }, [dispatch, getAllVehicles, getMappicoVehicleLists]);

  const getNavViewData = useCallback(async () => {
    if (isEmpty(nav_view)) {
      let { results } = await dispatch(getNavView());
      let view = results[0];

      // set data state
      setViewData(results[0]);

      // set marquee
      if (view.marquee_text !== '') setDisplayMarquee(true);

      // set interval
      setAdsDuration(view.ad_interval);
      // need fixed here

      // set media list
      let media = view.media_list.split(',');
      setMediaLists(media);

      // reset load
      dispatch(setReload(null));
    }
  }, [dispatch, getNavView, nav_view]);

  useEffect(() => {
    let elem = document.documentElement;

    if (elem.requestFullscreen) {
      elem.requestFullscreen();
    } else if (elem.msRequestFullscreen) {
      elem.msRequestFullscreen();
    } else if (elem.mozRequestFullScreen) {
      elem.mozRequestFullScreen();
    } else if (elem.webkitRequestFullscreen) {
      elem.webkitRequestFullscreen();
    }

    getMergeVehicleLists();
    getNavViewData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    console.log(reload);
    if (reload === 'RELOAD') location.reload();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [reload]);

  function getColor({ bus_line }) {
    switch (bus_line) {
      case 'yellow':
        return 'warning';
      case 'green':
        return 'success';
      case 'purple':
        return 'extra';
      case 'blue':
        return 'info';
      default:
        return 'primary';
    }
  }

  function getName({ bus_line }) {
    switch (bus_line) {
      case 'yellow':
        return 'สาย สีเหลือง';
      case 'green':
        return 'สาย สีเขียว';
      case 'purple':
        return 'สาย สีม่วง';
      case 'blue':
        return 'สาย สีฟ้า';
      default:
        return 'สาย สีแดง';
    }
  }

  function getRange({ time_left }) {
    if (time_left < 1) return 'ใกล้ถึงสถานี';

    return `${time_left} นาที`;
  }

  useEffect(() => {
    setInterval(() => setDateState(new Date()), 1000);
  }, []);

  useEffect(() => {
    setBusIn(public_vehicles_in);
  }, [public_vehicles_in]);

  const getBusInRow = useCallback((data) => {
    let findOnline = data.filter((i) => i.status === 'online');
    findOnline.sort((a, b) => parseFloat(a.time_left) - parseFloat(b.time_left));

    return findOnline.length < 1 ? (
      <TableRow>
        <TableCell colSpan={2} sx={{ textAlign: 'center' }}>
          <Typography variant='h5' sx={{ fontWeight: 400 }}>
            ไม่มีสายรถที่ใกล้ถึง
          </Typography>
        </TableCell>
      </TableRow>
    ) : (
      findOnline.map(
        (row, index) =>
          index < 3 && (
            <TableRow key={row.tracker_id} sx={{ maxHeight: '74px' }}>
              <TableCell>
                <div style={{ display: 'flex' }}>
                  <Box
                    variant='gradient'
                    bgColor={getColor(row)}
                    color={'white'}
                    width='2.5rem'
                    height='2.5rem'
                    marginRight='16px'
                    borderRadius='lg'
                    display='flex'
                    justifyContent='center'
                    alignItems='center'
                    shadow='md'
                  >
                    <Icon fontSize='small'>directions_bus</Icon>
                  </Box>
                  <Typography variant='h3' color={getColor(row)}>
                    {getName(row)}
                  </Typography>
                </div>
              </TableCell>
              <TableCell
                align='right'
                style={{
                  height: '74px',
                }}
              >
                <Typography variant='h3' color='dark' sx={{ margin: 0, padding: 0 }}>
                  {getRange(row)}
                </Typography>
              </TableCell>
            </TableRow>
          )
      )
    );
  }, []);

  const busInRow = useMemo(() => getBusInRow(busIn), [busIn, getBusInRow]);

  function hideMarquee(event) {
    if (event.elapsedTime > 0) setDisplayMarquee(false);
  }

  function getMilSec(duration) {
    let ms = duration * 1000;
    return ms;
  }

  const getScale = useCallback((value) => {
    if (value < 26) return pmVeryGood;
    if (value > 25) return pmGood;
    if (value > 37) return pmMedium;
    if (value > 50) return pmBad;

    return pmVeryBad;
  }, []);

  return (
    <Box sx={{ height: '100vh', width: 'auto' }}>
      {viewData === null ? (
        <Skeleton animation='wave' variant='rectangular' width='100%' height='100%' />
      ) : viewData?.toggle ? (
        <Box
          sx={{
            height: '100vh',
            width: 'auto',
            display: 'flex',
            justifyContent: 'center',
            alignItems: 'center',
          }}
        >
          <Typography variant='h1' color='primary'>
            {viewData.emergency_text}
          </Typography>
        </Box>
      ) : (
        <>
          <Grid
            container
            sx={{
              height: '12%',
              background: '#C00832',
            }}
          >
            <Stack
              direction='row'
              alignItems='center'
              justifyContent='space-between'
              px={3}
              sx={{ width: '100%' }}
            >
              <Stack direction='row' alignItems='center' spacing={2}>
                <Icon fontSize='large' sx={{ color: 'yellow !important' }}>
                  wb_sunny
                </Icon>

                <div
                  style={{
                    display: 'flex',
                    justifyContent: 'center',
                    alignItems: 'center',
                    marginRight: '24px',
                  }}
                >
                  <Typography variant='h2' color='white' sx={{ fontWeight: 500 }}>
                    {viewData?.temperature || 32}°
                  </Typography>
                  <Icon fontSize='medium' sx={{ color: 'white !important' }}>
                    device_thermostat
                  </Icon>
                </div>

                <Stack direction='row' alignItems='center' spacing={2}>
                  <Typography variant='h3' color='white'>
                    PM₂₅
                  </Typography>

                  <div
                    style={{
                      display: 'flex',
                      justifyContent: 'center',
                      alignItems: 'center',
                    }}
                  >
                    <Typography
                      variant='h2'
                      color='white'
                      sx={{ fontWeight: 500, marginRight: '6px' }}
                    >
                      {viewData?.pm25_dust || 112}
                    </Typography>

                    <img src={getScale(viewData?.pm25_dust)} width='38px' />
                  </div>
                </Stack>
              </Stack>

              <Stack direction='row'>
                <Box component='img' src={tuLogo} alt='Brand' width='5rem' mr={1} />
                <Stack alignItems='end' justifyContent='center' sx={{ minWidth: '150px' }}>
                  <Typography color='white' variant='h2'>
                    {dateState.toLocaleString('en-US', {
                      hour: 'numeric',
                      minute: 'numeric',
                      second: 'numeric',
                      hour12: false,
                    })}
                  </Typography>
                  <Typography color='white' variant='h5'>
                    {dateState.toLocaleDateString('th', {
                      day: 'numeric',
                      month: 'short',
                      year: 'numeric',
                    })}
                  </Typography>
                </Stack>
              </Stack>
            </Stack>
          </Grid>
          <Grid container sx={{ height: '76%' }}>
            <Grid item xs={7} sx={{ height: '100%' }}>
              <Box sx={{ height: '100%' }}>
                <MapComponent
                  bounds={bounds}
                  height='100%'
                  fullScreen={false}
                  geoJson={staticMap.data}
                  vehicles={public_vehicles_emit}
                  draggable={false}
                  currentLocation={{ stationName, coordinate: [lat, lon] }}
                  hideLayer
                />
              </Box>
            </Grid>
            <Grid item xs={5} sx={{ height: '100%', position: 'relative' }}>
              <Box sx={{ width: '100%', height: '46%', background: 'gray' }}>
                {mediaLists.length > 0 ? (
                  <Carousel
                    className='carousel-wrapper'
                    autoPlay={true}
                    infiniteLoop={true}
                    interval={getMilSec(adsDuration)}
                    dynamicHeight={true}
                    showThumbs={false}
                    showIndicators={false}
                    showArrows={false}
                  >
                    {mediaLists.map((item, index) => {
                      let type = item.split('.').pop().split('?')[0];

                      return (
                        <div
                          key={index}
                          style={{
                            width: '100%',
                            height: '100%',
                            backgroundImage: type === 'mp4' ? null : `url(${item})`,
                            backgroundSize: type === 'mp4' ? null : 'fit',
                            backgroundPosition: type === 'mp4' ? null : 'center',
                            backgroundRepeat: 'no-repeat',
                          }}
                        >
                          {type === 'mp4' && (
                            <iframe
                              width='100%'
                              height='100%'
                              src={`${item}`}
                              frameBorder='0'
                              allow='autoplay'
                              title='TU Announce'
                            />
                          )}
                        </div>
                      );
                    })}
                  </Carousel>
                ) : null}
              </Box>
              <Box
                sx={{
                  position: 'absolute',
                  height: 'auto',
                  display: 'flex',
                  alignItems: 'center',
                  top: '36.5%',
                  width: '100%',
                }}
              >
                {displayMarquee && (
                  <Marquee
                    loop={viewData?.ad_interval || 0}
                    gradient={false}
                    onFinish={(event) => hideMarquee(event)}
                    style={{
                      color: '#C00832',
                      background: 'rgba(255,255,255,0.65)',
                      fontSize: '1.7rem',
                    }}
                  >
                    {viewData?.marquee_text}
                  </Marquee>
                )}
              </Box>

              <Table sx={{ padding: 0, margin: 0, border: 'none' }}>
                {/* set height: 55% */}
                <TableHead>
                  <TableRow sx={{ background: '#C00832', height: '72px' }}>
                    <TableCell
                      align='left'
                      style={{ color: 'white', fontSize: '1.75rem', fontWeight: 500 }}
                    >
                      สายรถที่กำลังจะมาถึง
                    </TableCell>
                    <TableCell
                      align='right'
                      style={{ color: 'white', fontSize: '1.75rem', fontWeight: 500 }}
                    >
                      ระยะเวลา
                    </TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>{busInRow}</TableBody>
              </Table>
            </Grid>
          </Grid>
          <Grid container sx={{ height: '12%', background: 'white', overflow: 'hidden' }} px={3}>
            <Stack
              direction='row'
              alignItems='center'
              justifyContent='space-between'
              sx={{ width: '100%' }}
            >
              <Typography variant='h1' color='primary' sx={{ fontSize: '2.75rem' }}>
                {stationName}
              </Typography>
            </Stack>
          </Grid>
        </>
      )}
    </Box>
  );
});

SignView.displayName = 'NavView';

export default SignView;
