import { useState, useEffect, useCallback, useMemo } from 'react';
import { useParams } from 'react-router-dom';

import { useDispatch, useSelector } from 'react-redux';

import { Icon } from '@mui/material';
import Grid from '@mui/material/Grid';
import Stack from '@mui/material/Stack';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';

import Box from 'components/Box';
import Typography from 'components/Typography';

import MapComponent from 'pages/map';

import { useSocketPublic } from 'hooks';
import map from 'app/reducers/map';
import mapService from 'services/map.service';

// import static map geojson
import staticMap from 'utils/constants/map.json';

function getColor({ bus_line }) {
  switch (bus_line) {
    case 'yellow':
      return 'warning';
    case 'green':
      return 'success';
    case 'purple':
      return 'extra';
    case 'blue':
      return 'info';
    default:
      return 'primary';
  }
}

function getRange({ time_left }) {
  if (time_left < 1) return 'ใกล้ถึงสถานี';

  return `${time_left} นาที`;
}

const DebugView = () => {
  const dispatch = useDispatch();
  const { setPublicVehiclesEmit } = map.actions;
  const { getMappicoVehicleLists, getAllVehicles } = mapService;

  const { stationName, stationID, lat, lon } = useParams();
  const socket = useSocketPublic(stationID);

  const { public_vehicles_emit, public_vehicles_in } = useSelector((state) => state.map);

  const [busIn, setBusIn] = useState([]);

  useEffect(() => {
    return () => {
      socket?.close();
    };
  }, [socket]);

  useEffect(() => {
    setBusIn(public_vehicles_in);
  }, [public_vehicles_in]);

  const getMergeVehicleLists = useCallback(async () => {
    let { data } = await dispatch(getMappicoVehicleLists());
    let { results } = await dispatch(getAllVehicles());

    let allTracker = results.map((i) => {
      let filter = data.filter((obj) => obj.device_id == i.tracker_id);

      return { ...i, ...{ obd_data: {}, status: filter[0]?.status || 'offline' } };
    });

    dispatch(setPublicVehiclesEmit(allTracker));
  }, [dispatch, getAllVehicles, getMappicoVehicleLists, setPublicVehiclesEmit]);

  useEffect(() => {
    getMergeVehicleLists();
  }, [getMergeVehicleLists]);

  const getBusInRow = useCallback((data) => {
    let res = [...data];
    res.sort((a, b) => parseFloat(a.time_left) - parseFloat(b.time_left));

    return res.length < 1 ? (
      <TableRow>
        <TableCell colSpan={2} sx={{ textAlign: 'center' }}>
          <Typography variant='h5' sx={{ fontWeight: 400 }}>
            ไม่มีสายรถที่ใกล้ถึง
          </Typography>
        </TableCell>
      </TableRow>
    ) : (
      res.map((row) => (
        <TableRow key={row.tracker_id} sx={{ maxHeight: '74px' }}>
          <TableCell>
            <div style={{ display: 'flex', alignItems: 'center' }}>
              <Box
                variant='gradient'
                bgColor={getColor(row)}
                color={'white'}
                width='2.5rem'
                height='2.5rem'
                marginRight='16px'
                borderRadius='lg'
                display='flex'
                justifyContent='center'
                alignItems='center'
                shadow='md'
              >
                <Icon fontSize='small'>directions_bus</Icon>
              </Box>
              <Typography variant='h5' color='secondary' mr={2}>
                {row.tracker_id}
              </Typography>
              {row.status === 'offline' && <Icon color='error'>wifi_off</Icon>}
            </div>
          </TableCell>
          <TableCell
            align='right'
            style={{
              height: '74px',
            }}
          >
            <Typography variant='h5' color='dark' sx={{ margin: 0, padding: 0 }}>
              {getRange(row)}
            </Typography>
          </TableCell>
        </TableRow>
      ))
    );
  }, []);

  const busInRow = useMemo(() => getBusInRow(busIn), [busIn, getBusInRow]);

  return (
    <Box sx={{ height: '100vh', width: 'auto' }}>
      <Stack
        direction='row'
        alignItems='center'
        spacing={2}
        p={2}
        sx={{ height: '5%', background: 'lightgray' }}
      >
        <Typography variant='body' color='secondary'>
          Debug Mode
        </Typography>
        <Typography variant='body'>
          Station-Id: {stationName}-{stationID}
        </Typography>
      </Stack>

      <Grid container sx={{ height: '65%' }}>
        <Grid item xs={7} sx={{ height: '100%' }}>
          <MapComponent
            zoom={15.4}
            center={[14.0719, 100.6056]}
            height='100%'
            fullScreen={false}
            geoJson={staticMap.data}
            vehicles={public_vehicles_emit}
            draggable={false}
            currentLocation={{ stationName, coordinate: [lat, lon] }}
          />
        </Grid>
        <Grid item xs={5} sx={{ height: '100%', overflow: 'scroll' }}>
          <Table sx={{ padding: 0, margin: 0, border: 'none', height: '100%' }}>
            <TableHead>
              <TableRow sx={{ background: 'gray', height: '72px' }}>
                <TableCell align='left' style={{ color: 'white' }}>
                  สายรถที่กำลังจะมาถึง
                </TableCell>
                <TableCell align='right' style={{ color: 'white' }}>
                  ระยะเวลา
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>{busInRow}</TableBody>
          </Table>
        </Grid>
      </Grid>

      <Grid container sx={{ height: '30%', background: 'black', overflow: 'scroll' }}>
        <Typography variant='body' color='white'>
          {JSON.stringify(public_vehicles_in)}
        </Typography>
      </Grid>
    </Box>
  );
};

export default DebugView;
