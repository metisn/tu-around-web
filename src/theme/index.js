// @mui material components
import { createTheme } from '@mui/material/styles';

// Material Dashboard 2 PRO React base styles
import colors from 'theme/base/colors';
import breakpoints from 'theme/base/breakpoints';
import typography from 'theme/base/typography';
import boxShadows from 'theme/base/boxShadows';
import borders from 'theme/base/borders';
import globals from 'theme/base/globals';

// Material Dashboard 2 PRO React helper functions
import boxShadow from 'theme/functions/boxShadow';
import hexToRgb from 'theme/functions/hexToRgb';
import linearGradient from 'theme/functions/linearGradient';
import pxToRem from 'theme/functions/pxToRem';
import rgba from 'theme/functions/rgba';

// Material Dashboard 2 PRO React components base styles for @mui material components
import container from 'theme/components/container';
import sidenav from 'theme/components/sidenav';
import button from 'theme/components/button';
import iconButton from 'theme/components/iconButton';
import list from 'theme/components/list';
import listItem from 'theme/components/list/listItem';
import listItemText from 'theme/components/list/listItemText';
import divider from 'theme/components/divider';
import appBar from 'theme/components/appBar';
import icon from 'theme/components/icon';
import svgIcon from 'theme/components/svgIcon';
import link from 'theme/components/link';
import card from 'theme/components/card';
import cardContent from 'theme/components/card/cardContent';
import formControlLabel from 'theme/components/form/formControlLabel';
import formLabel from 'theme/components/form/formLabel';
import input from 'theme/components/input/input';
import inputLabel from 'theme/components/input/inputLabel';
import inputOutlined from 'theme/components/input/inputOutlined';
import textField from 'theme/components/input/textField';
import switchButton from 'theme/components/switchButton';
import autocomplete from 'theme/components/form/autocomplete';

export default createTheme({
  breakpoints: { ...breakpoints },
  palette: { ...colors },
  typography: { ...typography },
  boxShadows: { ...boxShadows },
  borders: { ...borders },
  functions: {
    boxShadow,
    hexToRgb,
    linearGradient,
    pxToRem,
    rgba,
  },
  components: {
    MuiCssBaseline: {
      styleOverrides: {
        ...globals,
        ...container,
      },
    },
    MuiDrawer: { ...sidenav },
    MuiList: { ...list },
    MuiListItem: { ...listItem },
    MuiListItemText: { ...listItemText },
    MuiButton: { ...button },
    MuiIconButton: { ...iconButton },
    MuiDivider: { ...divider },
    MuiAppBar: { ...appBar },
    MuiIcon: { ...icon },
    MuiSvgIcon: { ...svgIcon },
    MuiLink: { ...link },
    MuiCard: { ...card },
    MuiCardContent: { ...cardContent },
    MuiFormControlLabel: { ...formControlLabel },
    MuiFormLabel: { ...formLabel },
    MuiInput: { ...input },
    MuiInputLabel: { ...inputLabel },
    MuiOutlinedInput: { ...inputOutlined },
    MuiTextField: { ...textField },
    MuiSwitch: { ...switchButton },
    MuiAutocomplete: { ...autocomplete },
  },
});
